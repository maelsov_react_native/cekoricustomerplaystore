import ACTION_TYPES from "../actions";

const initialState = {
  activeLanguage: 'EN'
};

const languageReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPES.CHANGE_LANGUAGE:
      return {
        ...state,
        activeLanguage: action.activeLanguage
      };
      break;
    default:
      return state;
  }
};

export default languageReducer;