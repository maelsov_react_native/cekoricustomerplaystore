import { createStore, combineReducers, applyMiddleware } from "redux";
import languageReducer from "../reducers/languageReducer";

// Place to store the variable
const rootReducer = combineReducers({
  languageOperation: languageReducer
});

const configureStore = (() => {
  return createStore(
    rootReducer
    // applyMiddleware(logger)
  );
});

export default configureStore;