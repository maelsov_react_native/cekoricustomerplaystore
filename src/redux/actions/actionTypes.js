import ACTION_TYPES from '.';

export const changeLanguage = activeLanguage => {
    return {
        type: ACTION_TYPES.CHANGE_LANGUAGE,
        activeLanguage: activeLanguage
    }
}
