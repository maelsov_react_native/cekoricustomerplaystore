export default {
    EN: {
        TEXT_CHECK_PRODUCT : 'Check your product originality at CekOri ',

        INPUT_PHONE_NUMBER: 'Phone Number',
        INPUT_EMAIL: 'Email Address',
        INPUT_PASSWORD: 'Password',
        INPUT_CONFIRM_PASSWORD: 'Confirm Password',
        INPUT_FIRST_NAME: 'First Name',
        INPUT_LAST_NAME: 'Last Name',

        HELPER_REQUIRED: 'Required',
        HELPER_OPTIONAL: 'Optional',
        HELPER_PASSWORD: 'Passwords 8 to 20 characters and contain a combination of capital letters, lowercase letters, numbers and special characters',

        TOAST_WARNING_EMPTY_PHONE_NUMBER : 'You have not entered phone number',
        TOAST_WARNING_EMPTY_PASSWORD : 'You have not entered password',
        TOAST_WARNING_EMPTY_FIRST_NAME : 'You have not entered first name',
        TOAST_WARNING_EMPTY_LAST_NAME : 'You have not entered last name',
        TOAST_WARNING_EMPTY_EMAIL : 'You have not entered email',
        TOAST_WARNING_NOT_VALID_EMAIL : 'Email format is not  valid',
        TOAST_WARNING_EMPTY_PASSWORD_CONFIRMATION : 'You have not entered password confirmation',
        TOAST_WARNING_NOT_VALID_PASSWORD : "Password doesn't match the criteria",
        TOAST_WARNING_DIFFERENT_PASSWORD : 'Password confirmation is different with password',
        TOAST_ERROR_DEFAULT : 'Connection not available',

        TOAST_ERROR_ER01: 'Validation Error',
    },
    ID: {
        TEXT_CHECK_PRODUCT : 'Cek originalitas barangmu di CekOri ',
        
        INPUT_PHONE_NUMBER: 'Nomor Telepon',
        INPUT_EMAIL: 'Alamat Email',
        INPUT_PASSWORD: 'Kata Sandi',
        INPUT_CONFIRM_PASSWORD: 'Ulangi Kata Sandi',
        INPUT_FIRST_NAME: 'Nama Depan',
        INPUT_LAST_NAME: 'Nama Belakang',

        HELPER_REQUIRED: 'Dibutuhkan',
        HELPER_OPTIONAL: 'Opsional',
        HELPER_PASSWORD: 'Kata Sandi 8 hingga 20 karakter dan mengandung kombinasi huruf kapital, huruf hecil, angka dan karakter spesial',

        TOAST_WARNING_EMPTY_PHONE_NUMBER : 'Anda belum menginput nomor telepon',
        TOAST_WARNING_EMPTY_PASSWORD : 'Anda belum menginput kata sandi',
        TOAST_WARNING_EMPTY_FIRST_NAME : 'Anda belum menginput nama depan',
        TOAST_WARNING_EMPTY_LAST_NAME : 'Anda belum menginput nama belakang',
        TOAST_WARNING_EMPTY_EMAIL : 'Anda belum menginput email',
        TOAST_WARNING_NOT_VALID_EMAIL : 'Format email tidak valid',
        TOAST_WARNING_EMPTY_PASSWORD_CONFIRMATION : 'Anda belum menginput kata sandi konfirmasi',
        TOAST_WARNING_NOT_VALID_PASSWORD : 'Kata sandi tidak sesuai ketentuan',
        TOAST_WARNING_DIFFERENT_PASSWORD : 'Kata sandi konfirmasi Anda berbeda dengan kata sandi',
        TOAST_ERROR_DEFAULT : 'Koneksi tidak tersedia',

        TOAST_ERROR_ER01: 'Kesalahan Validasi',
    },
}