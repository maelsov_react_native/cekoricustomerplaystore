export default {
    EN: {
        TEXT_DIALING_CODE_HEADER: 'Choose Dialing Code',
        TEXT_LANGUAGE_HEADER: 'Choose Language',
    },
    ID: {
        TEXT_DIALING_CODE_HEADER: 'Pilih Kode Panggilan',
        TEXT_LANGUAGE_HEADER: 'Pilih Bahasa',
    },
}