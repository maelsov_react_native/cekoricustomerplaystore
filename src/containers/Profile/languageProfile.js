export default {
    EN: {
        TEXT_PROFILE_HEADER: 'PROFILE',
        TEXT_EDIT_PROFILE_HEADER: 'EDIT PROFILE',

        TEXT_PHONE: 'Phone',
        TEXT_EMAIL: 'Email',
        TEXT_GENDER: 'Gender',
        TEXT_ADDRESS: 'Address',

        TEXT_MODAL_CHOOSE_PHOTO_TITLE: 'Choose profile photo',
        BUTTON_MODAL_CHOOSE_PHOTO_TAKE_PHOTO: 'Take photo',
        BUTTON_MODAL_CHOOSE_PHOTO_CHOOSE_GALLERY: 'Choose from gallery',
        BUTTON_MODAL_CHOOSE_PHOTO_CANCEL: 'Cancel',
        TEXT_MODAL_CHOOSE_PHOTO_DENIED_TITLE: 'Access needed to edit photo',
        TEXT_MODAL_CHOOSE_PHOTO_DENIED_BODY: 'Allow access to take photo from camera or gallery',
        BUTTON_MODAL_CHOOSE_PHOTO_DENIED_OPEN_SETTING: 'Open Setting',
        BUTTON_MODAL_CHOOSE_PHOTO_DENIED_OK: 'Close',

        BUTTON_CHANGE_PHOTO: 'Edit Photo',
        BUTTON_CHANGE_PROFILE: 'Edit Profile',

        TOAST_SUCCESS_CHANGE_PHOTO: 'Edit photo success',
        TOAST_WARNING_CHANGE_PHOTO: 'Edit photo failed',

        TOAST_ERROR_CHOOSE_PHOTO: 'Error while choosing photo'
    },
    ID: {
        TEXT_PROFILE_HEADER: 'PROFIL',
        TEXT_EDIT_PROFILE_HEADER: 'UBAH PROFIL',

        TEXT_PHONE: 'Telepon',
        TEXT_EMAIL: 'Email',
        TEXT_GENDER: 'Jenis Kelamin',
        TEXT_ADDRESS: 'Alamat',

        TEXT_MODAL_CHOOSE_PHOTO_TITLE: 'Pilih foto profil',
        BUTTON_MODAL_CHOOSE_PHOTO_TAKE_PHOTO: 'Ambil foto',
        BUTTON_MODAL_CHOOSE_PHOTO_CHOOSE_GALLERY: 'Pilih dari galeri',
        BUTTON_MODAL_CHOOSE_PHOTO_CANCEL: 'Batal',
        TEXT_MODAL_CHOOSE_PHOTO_DENIED_TITLE: 'Akses dibutuhkan untuk mengubah foto',
        TEXT_MODAL_CHOOSE_PHOTO_DENIED_BODY: 'Izinkan akses untuk mengambil foto dari kamera atau galeri',
        BUTTON_MODAL_CHOOSE_PHOTO_DENIED_OPEN_SETTING: 'Buka Pengaturan',
        BUTTON_MODAL_CHOOSE_PHOTO_DENIED_OK: 'Tutup',

        BUTTON_CHANGE_PHOTO: 'Ubah Foto',
        BUTTON_CHANGE_PROFILE: 'Ubah Profil',

        TOAST_SUCCESS_CHANGE_PHOTO: 'Foto profil berhasil diubah',
        TOAST_WARNING_CHANGE_PHOTO: 'Foto profil gagal diubah',

        TOAST_ERROR_CHOOSE_PHOTO: 'Terdapat kesalahan saat mengambil foto'
    },
}