import React, { Component, useEffect, useRef, useState } from 'react';
import { View, Image, TouchableOpacity, ScrollView, Text, Dimensions, ActivityIndicator, StyleSheet } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { Colors, Metrics, Fonts, StorageKey } from '../../GlobalConfig';
import { getUserData, wait } from '../../GlobalFunction';
import { modelUser } from '../../models/User';
import languageProfile from './languageProfile';
import GlobalLanguage from '../../GlobalLanguage';

import CustomToast from '../../components/CustomToast';

const ProfileScreen = (props) => {
    const { activeLanguage } = props
    const [isLoading, setIsLoading] = useState(false)
    const [user_first_name, setUserFirstName] = useState('')
    const [user_last_name, setUserLastName] = useState('')
    const [user_email, setUserEmail] = useState('')
    const [user_phone, setUserPhone] = useState('')
    const [user_address, setUserAddress] = useState('')
    const [user_image, setUserImage] = useState('')
    const [user_sex, setUserSex] = useState('')
    const [activePrefix, setActivePrefix] = useState('+62')
    const toast = useRef(null)

    useEffect(() => {
        fetchData()
    }, [])

    useEffect(() => {
        if(props.lastUpdate){
            fetchData()
        }
    }, [props.lastUpdate])

    useEffect(() => {
        if (props.lastUpdatePhonePrefix) {
            setActivePrefix(props.activePrefix)
            setIsModalVisible(true)
        }
    }, [props.lastUpdatePhonePrefix])




    useEffect(() => {
        if (props.activeLanguage) {
            wait(0).then(() => Actions.refresh({ title: languageProfile[activeLanguage].TEXT_PROFILE_HEADER }))
        }
    }, [props.activeLanguage])

    const uploadImage = (imageData) => {
        console.log(imageData)
        getUserData().then(result => {
            const { user_id } = result
            setIsLoading(true)
            const formdata = new FormData()
            formdata.append('user_id', user_id)
            formdata.append('user_phone', user_phone)
            formdata.append('user_image', {
                uri: imageData,
                type: 'image/jpeg', // or photo.type
                name: 'profilePicture'
            })
            modelUser.updateAvatar(formdata, res => {
                const { status, result } = res
                switch (status) {
                    case 200:
                        toast.current.ShowToastFunction("success", languageProfile[activeLanguage].TOAST_SUCCESS_CHANGE_PHOTO)
                        fetchData()
                        break;
                    default:
                        toast.current.ShowToastFunction("warning", languageProfile[activeLanguage].TOAST_WARNING_CHANGE_PHOTO)
                        setIsLoading(false)
                        break;
                }
            })
                .catch((err) => {
                    console.log(err)
                    toast.current.ShowToastFunction("error", GlobalLanguage[activeLanguage].TOAST_ERROR_DEFAULT)
                    setIsLoading(false)
                })
        })
    }

    const fetchData = () => {
        getUserData().then(result => {
            const { user_id } = result
            setIsLoading(true)
            modelUser.getProfile(user_id, res => {
                const { status, result } = res
                console.log(res)
                switch (status) {
                    case 200:
                        setUserFirstName(result.message.user_first_name)
                        setUserLastName(result.message.user_last_name)
                        setUserEmail(result.message.user_email ? result.message.user_email : '-')
                        setUserPhone(result.message.user_phone ? result.message.user_phone : '-')
                        setUserImage(result.message.user_image)
                        setUserSex(result.message.user_sex ? result.message.user_sex : '-')
                        setUserAddress(result.message.user_address ? result.message.user_address : '-')
                        break;
                    default:
                        break;
                }
                setIsLoading(false)
            })
                .catch((error) => {
                    toast.current.ShowToastFunction("error", GlobalLanguage[activeLanguage].TOAST_ERROR_DEFAULT)
                    setIsLoading(false)
                    console.log(error)
                })
        })
    }

    const handleChangePicturePress = () => {
        const options = {
            title: languageProfile[activeLanguage].TEXT_MODAL_CHOOSE_PHOTO_TITLE,
            maxWidth: 500,
            maxHeight: 500,
            mediaType: 'photo',
            allowsEditing: true,
            noData: true,
            takePhotoButtonTitle: languageProfile[activeLanguage].BUTTON_MODAL_CHOOSE_PHOTO_TAKE_PHOTO,
            chooseFromLibraryButtonTitle: languageProfile[activeLanguage].BUTTON_MODAL_CHOOSE_PHOTO_CHOOSE_GALLERY,
            cancelButtonTitle: languageProfile[activeLanguage].BUTTON_MODAL_CHOOSE_PHOTO_CANCEL,
            permissionDenied: {
                title: languageProfile[activeLanguage].TEXT_MODAL_CHOOSE_PHOTO_DENIED_TITLE,
                text: languageProfile[activeLanguage].TEXT_MODAL_CHOOSE_PHOTO_DENIED_BODY,
                reTryTitle: languageProfile[activeLanguage].BUTTON_MODAL_CHOOSE_PHOTO_DENIED_OPEN_SETTING,
                okTitle: languageProfile[activeLanguage].BUTTON_MODAL_CHOOSE_PHOTO_DENIED_OK
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            if (response.error) {
                console.log('ImagePicker Error: ', response.error);
                toast.current.ShowToastFunction("error", languageProfile[activeLanguage].TOAST_ERROR_CHOOSE_PHOTO)
            }
            else if (response.didCancel) { }
            else {
                uploadImage(response.uri)
            }
        })
    }

    const handleEditProfile = () => {
        getUserData().then(result => {
            const { user_id } = result
            Actions.editProfile({
                title: languageProfile[activeLanguage].TEXT_EDIT_PROFILE_HEADER,
                user_id: user_id,
                user_first_name: user_first_name,
                user_last_name: user_last_name,
                user_email: user_email,
                user_address: user_address,
                user_sex: user_sex
            })
        })
    }


    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            <ScrollView contentContainerStyle={{ paddingVertical: Metrics.SAFE_AREA }}>
                <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{
                        height: Metrics.SCREEN_HEIGHT * 0.25,
                        aspectRatio: 1,
                        borderRadius: Metrics.SCREEN_HEIGHT * 0.125,
                        borderWidth: 2,
                        borderColor: Colors.WHITE,
                        backgroundColor: Colors.WHITE,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 1,
                        },
                        shadowOpacity: 0.22,
                        shadowRadius: 2.22,

                        elevation: 3,
                    }}>
                        <View style={{ overflow: 'hidden', flex: 1, borderRadius: Metrics.SCREEN_HEIGHT * 0.125 }}>
                            {isLoading ? (
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <ActivityIndicator color={Colors.RED} size='large' />
                                </View>
                            ) : <Image resizeMode='cover' source={{ uri: user_image ? user_image : null }} style={styles.profileImg} />}
                        </View>
                    </View>
                </View>
                <View style={{ padding: Metrics.SAFE_AREA, width: '100%' }}>
                    <View style={{ width: '80%', alignSelf: 'center', marginBottom: 20 }}>
                        <Text style={styles.nameTxt}>{user_first_name} {user_last_name}</Text>
                    </View>
                    <View style={{ width: '100%' }}>
                        <View style={{ paddingVertical: 10, paddingHorizontal: Metrics.SAFE_AREA, backgroundColor: Colors.GRAY, flexDirection: 'row' }}>
                            <View style={{ width: 120, justifyContent: 'center' }}>
                                <Text style={styles.titleTxt}>{languageProfile[activeLanguage].TEXT_PHONE}</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={styles.valueTxt}>{activePrefix}{user_phone}</Text>
                            </View>
                        </View>
                        <View style={{ paddingVertical: 10, paddingHorizontal: Metrics.SAFE_AREA, backgroundColor: Colors.GRAY_LIGHT, flexDirection: 'row' }}>
                            <View style={{ width: 120, justifyContent: 'center' }}>
                                <Text style={styles.titleTxt}>{languageProfile[activeLanguage].TEXT_EMAIL}</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={styles.valueTxt}>{user_email}</Text>
                            </View>
                        </View>
                        <View style={{ paddingVertical: 10, paddingHorizontal: Metrics.SAFE_AREA, backgroundColor: Colors.GRAY, flexDirection: 'row' }}>
                            <View style={{ width: 120, justifyContent: 'center' }}>
                                <Text style={styles.titleTxt}>{languageProfile[activeLanguage].TEXT_GENDER}</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={styles.valueTxt}>{user_sex === "0" ? 'Wanita' : user_sex === "1" ? "Pria" : user_sex}</Text>
                            </View>
                        </View>
                        <View style={{ paddingVertical: 10, paddingHorizontal: Metrics.SAFE_AREA, backgroundColor: Colors.GRAY_LIGHT, flexDirection: 'row' }}>
                            <View style={{ width: 120, justifyContent: 'center' }}>
                                <Text style={styles.titleTxt}>{languageProfile[activeLanguage].TEXT_ADDRESS}</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={styles.valueTxt}>{user_address}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
            <View style={{ height: 70, width: '100%', flexDirection: 'row' }}>
                <TouchableOpacity
                    onPress={handleChangePicturePress}
                    disabled={isLoading}
                    style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: user_phone ? Colors.BLACK : Colors.GRAY }}>
                    <Text style={{ fontFamily: Fonts.APEX_MEDIUM, color: '#fff', fontSize: 18 }}>{languageProfile[activeLanguage].BUTTON_CHANGE_PHOTO}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={handleEditProfile}
                    disabled={isLoading}
                    style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: user_phone ? Colors.RED : Colors.GRAY }}>
                    <Text style={{ fontFamily: Fonts.APEX_MEDIUM, color: '#fff', fontSize: 18 }}>{languageProfile[activeLanguage].BUTTON_CHANGE_PROFILE}</Text>
                </TouchableOpacity>
            </View>
            <CustomToast ref={toast} />
        </View>
    );
}

const styles = StyleSheet.create({
    profileImg: {
        flex: 1,
    },
    cameraIcon: {
        width: 60,
        height: 60,
        position: 'absolute',
        bottom: -30,
        right: 30,
        borderRadius: 30,
        backgroundColor: Colors.RED,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: Colors.WHITE,
        borderWidth: 1
    },

    nameTxt: {
        color: Colors.BLACK,
        fontFamily: Fonts.APEX_BOLD,
        fontSize: 24,
        textAlign: 'center'
    },

    titleTxt: {
        color: Colors.BLACK,
        fontFamily: Fonts.APEX_MEDIUM,
        fontSize: 16,
    },

    valueTxt: {
        fontSize: 14,
    },

});

const mapStateToProps = state => {
    return {
        activeLanguage: state.languageOperation.activeLanguage
    };
};

export default connect(mapStateToProps)(ProfileScreen);