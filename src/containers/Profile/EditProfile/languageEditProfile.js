export default {
    EN: {
        TEXT_MALE: 'Male',
        TEXT_FEMALE: 'Female',

        INPUT_GENDER: 'Gender',
        INPUT_ADDRESS: 'Address',

        BUTTON_SAVE: 'Save',
        BUTTON_CANCEL: 'Cancel Edit',

        TOAST_SUCCESS_EDIT_PROFILE: 'Edit profile success',
        TOAST_WARNING_EDIT_PROFILE: 'Edit profile failed',

        TEXT_MODAL_SUBMIT_CHANGE_TITLE: 'Submit Change ?',
        TEXT_MODAL_SUBMIT_CHANGE_BODY: 'Tap submit to save current change',
        BUTTON_MODAL_SUBMIT_CHANGE_CANCEL: 'No',
        BUTTON_MODAL_SUBMIT_CHANGE_SAVE: 'Submit',

        TEXT_MODAL_CANCEL_CHANGE_TITLE: 'Cancel Change ?',
        TEXT_MODAL_CANCEL_CHANGE_BODY: 'Changes you made will not be saved',
        BUTTON_MODAL_CANCEL_CHANGE_CANCEL: 'No',
        BUTTON_MODAL_CANCEL_CHANGE_SAVE: 'Cancel',

        TOAST_SEX_UNSELECT : 'Gender must be selected',
    },
    ID: {
        TEXT_MALE: 'Pria',
        TEXT_FEMALE: 'Wanita',

        INPUT_GENDER: 'Jenis Kelamin',
        INPUT_ADDRESS: 'Alamat',

        BUTTON_SAVE: 'Simpan',
        BUTTON_CANCEL: 'Batal Perubahan',

        TOAST_SUCCESS_EDIT_PROFILE: 'Profil berhasil diubah',
        TOAST_WARNING_EDIT_PROFILE: 'Profil gagal diubah',

        TEXT_MODAL_SUBMIT_CHANGE_TITLE: 'Simpan Perubahan ?',
        TEXT_MODAL_SUBMIT_CHANGE_BODY: 'Tekan simpan untuk menyimpan perubahan saat ini',
        BUTTON_MODAL_SUBMIT_CHANGE_CANCEL: 'Tidak',
        BUTTON_MODAL_SUBMIT_CHANGE_SAVE: 'Simpan',

        TEXT_MODAL_CANCEL_CHANGE_TITLE: 'Batal Perubahan ?',
        TEXT_MODAL_CANCEL_CHANGE_BODY: 'Perubahan yang Anda lakukan tidak akan tersimpan',
        BUTTON_MODAL_CANCEL_CHANGE_CANCEL: 'Tidak',
        BUTTON_MODAL_CANCEL_CHANGE_SAVE: 'Batal',

        TOAST_SEX_UNSELECT : 'Jenis kelamin harus dipilih',
    },
}