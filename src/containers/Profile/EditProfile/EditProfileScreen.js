import React, { useRef, useState } from 'react';
import { View, TouchableOpacity, ScrollView, Text, Alert, ActivityIndicator, TextInput, KeyboardAvoidingView, Platform, StyleSheet } from 'react-native';
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { Colors, Metrics, Fonts } from '../../../GlobalConfig';
import { wait } from '../../../GlobalFunction';
import { modelUser } from '../../../models/User';
import GlobalLanguage from '../../../GlobalLanguage';
import languageEditProfile from './languageEditProfile';

import FloatingLabelInput from '../../../components/FloatingLabelInput';
import CustomToast from '../../../components/CustomToast';

const EditProfileScreen = (props) => {
    const { activeLanguage } = props
    const [isLoading, setIsLoading] = useState(false)
    const [user_id, setUserId] = useState(props.user_id)
    const [user_first_name, setUserFirstName] = useState(props.user_first_name)
    const [user_last_name, setUserLastName] = useState(props.user_last_name)
    const [user_email, setUserEmail] = useState(props.user_email)
    const [user_address, setUserAddress] = useState(props.user_address)
    const [user_sex, setUserSex] = useState(props.user_sex == "-" ? null : props.user_sex)
    const toast = useRef(null)

    const handleSubmit = () => {
        setIsLoading(true)
        const formdata = new FormData();
        formdata.append('user_id', user_id)
        formdata.append('user_first_name', user_first_name)
        formdata.append('user_last_name', user_last_name)
        formdata.append('user_email', user_email)
        formdata.append('user_address', user_address)
        formdata.append('user_sex', user_sex)
        formdata.append('user_fcm_token', '-')

        modelUser.updateProfile(formdata, res => {
            const { status, result } = res
            switch (status) {
                case 200:
                    toast.current.ShowToastFunction("success", languageEditProfile[activeLanguage].TOAST_SUCCESS_EDIT_PROFILE)
                    wait(2000).then(() => {
                        setIsLoading(false)
                        Actions.pop()
                        wait(0).then(() => Actions.refresh({ lastUpdate: new Date }))
                    })
                    break;
                default:
                    toast.current.ShowToastFunction("warning", `-${status}-\n${languageEditProfile[activeLanguage].TOAST_WARNING_EDIT_PROFILE}`)
                    console.warn(result.message)
                    setIsLoading(false)
                    break;
            }
        })
            .catch((error) => {
                console.log(error)
                toast.current.ShowToastFunction("error", GlobalLanguage[activeLanguage].TOAST_ERROR_DEFAULT)
                setIsLoading(false)
            })
    }

    const submitChanges = () => {
        if (!user_first_name) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_EMPTY_FIRST_NAME)
        }
        else if (!user_email) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_EMPTY_EMAIL)
        }
        else if (!user_sex) {
            toast.current.ShowToastFunction("warning", languageEditProfile[activeLanguage].TOAST_SEX_UNSELECT)
        }
        else {
            Alert.alert(
                languageEditProfile[activeLanguage].TEXT_MODAL_SUBMIT_CHANGE_TITLE,
                languageEditProfile[activeLanguage].TEXT_MODAL_SUBMIT_CHANGE_BODY,
                [
                    {
                        text: languageEditProfile[activeLanguage].BUTTON_MODAL_SUBMIT_CHANGE_CANCEL,
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    {
                        text: languageEditProfile[activeLanguage].BUTTON_MODAL_SUBMIT_CHANGE_SAVE,
                        onPress: handleSubmit
                    },
                ],
                { cancelable: false },
            )
        }
    }

    const cancelChanges = () => {
        Alert.alert(
            languageEditProfile[activeLanguage].TEXT_MODAL_CANCEL_CHANGE_TITLE,
            languageEditProfile[activeLanguage].TEXT_MODAL_CANCEL_CHANGE_BODY,
            [
                {
                    text: languageEditProfile[activeLanguage].BUTTON_MODAL_CANCEL_CHANGE_CANCEL,
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: languageEditProfile[activeLanguage].BUTTON_MODAL_CANCEL_CHANGE_SAVE,
                    onPress: () => Actions.pop()
                },
            ],
            { cancelable: false },
        )
    }
    

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS == 'ios' && 'position'}
            style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            <ScrollView contentContainerStyle={{ padding: Metrics.SAFE_AREA }}>
                <FloatingLabelInput
                    label={GlobalLanguage[activeLanguage].INPUT_FIRST_NAME}
                    value={user_first_name}
                    onChangeText={setUserFirstName}
                    helperText={GlobalLanguage[activeLanguage].HELPER_REQUIRED}
                    autoCompleteType="name"
                    bottomOnly
                    maxLength={50}
                />
                <FloatingLabelInput
                    label={GlobalLanguage[activeLanguage].INPUT_LAST_NAME}
                    value={user_last_name}
                    onChangeText={setUserLastName}
                    bottomOnly
                    maxLength={50}
                />
                <FloatingLabelInput
                    label={GlobalLanguage[activeLanguage].INPUT_EMAIL}
                    value={user_email}
                    keyboardType="email-address"
                    helperText={GlobalLanguage[activeLanguage].HELPER_REQUIRED}
                    autoCompleteType="email"
                    onChangeText={setUserEmail}
                    bottomOnly
                    maxLength={50}
                />
                <View style={{ marginLeft: Metrics.SAFE_AREA, paddingLeft: Metrics.SAFE_AREA, marginTop: 10 }}>
                    <Text style={{ color: Colors.RED, fontFamily: Fonts.APEX_MEDIUM, fontSize: 12 }}>{languageEditProfile[activeLanguage].INPUT_GENDER}</Text>
                </View>
                <View style={{ height: Metrics.SCREEN_HEIGHT * 0.1, width: '100%', flexDirection: 'row', justifyContent: 'space-around', marginVertical: 10 }}>
                    <TouchableOpacity
                        onPress={() => setUserSex('1')}
                        style={[
                            { height: '100%', aspectRatio: 1, borderWidth: 2, alignItems: 'center', justifyContent: 'space-around' },
                            user_sex == 1 ? { borderColor: Colors.RED } : { borderColor: Colors.GRAY }
                        ]}>
                        <SimpleLineIcons name="symbol-male" color={user_sex == 1 ? Colors.RED : Colors.GRAY} size={24} />
                        <Text style={[styles.genderText, user_sex == 1 ? { color: Colors.RED } : { color: Colors.GRAY }]}>{languageEditProfile[activeLanguage].TEXT_MALE}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setUserSex('0')}
                        style={[
                            { height: '100%', aspectRatio: 1, borderWidth: 2, alignItems: 'center', justifyContent: 'space-around' },
                            user_sex == 0 ? { borderColor: Colors.RED } : { borderColor: Colors.GRAY }
                        ]}>
                        <SimpleLineIcons name="symbol-female" color={user_sex == 0 ? Colors.RED : Colors.GRAY} size={24} />
                        <Text style={[styles.genderText, user_sex == 0 ? { color: Colors.RED } : { color: Colors.GRAY }]}>{languageEditProfile[activeLanguage].TEXT_FEMALE}</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ marginLeft: Metrics.SAFE_AREA, paddingLeft: Metrics.SAFE_AREA, marginTop: 10 }}>
                    <Text style={{ color: Colors.RED, fontFamily: Fonts.APEX_MEDIUM, fontSize: 12 }}>{languageEditProfile[activeLanguage].INPUT_ADDRESS}</Text>
                </View>
                <View style={{ paddingHorizontal: Metrics.SAFE_AREA }}>
                    <TextInput
                        placeholderTextColor={Colors.GRAY}
                        selectionColor={Colors.RED}
                        style={[{ textAlignVertical: 'bottom', alignItems: 'flex-end', minHeight: 40, maxHeight: 100, width: '100%', borderBottomWidth: 1, borderBottomColor: Colors.GRAY, paddingHorizontal: Metrics.SAFE_AREA }, Platform.OS == 'ios' && { lineHeight: 20 }]}
                        value={user_address}
                        onChangeText={setUserAddress}
                        multiline
                        numberOfLines={3}
                    />
                </View>
                <TouchableOpacity
                    disabled={isLoading}
                    onPress={submitChanges}
                    style={[{ height: 60, width: 150, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: 30, marginVertical: Metrics.SAFE_AREA }, isLoading ? { backgroundColor: Colors.GRAY } : { backgroundColor: Colors.RED }]}>
                    {isLoading ? <ActivityIndicator size="small" color={Colors.WHITE} /> :
                        <Text style={{ fontFamily: Fonts.APEX_MEDIUM, color: '#fff', fontSize: 18 }}>{languageEditProfile[activeLanguage].BUTTON_SAVE}</Text>}
                </TouchableOpacity>
                <TouchableOpacity
                    disabled={isLoading}
                    onPress={cancelChanges}
                    style={{ padding: Metrics.SAFE_AREA, width: 200, justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                    <Text style={isLoading ? {opacity:0} :{ fontFamily: Fonts.APEX_MEDIUM, color: Colors.RED, fontSize: 18, letterSpacing: 1 }}>{languageEditProfile[activeLanguage].BUTTON_CANCEL}</Text>
                </TouchableOpacity>
            </ScrollView>
            <CustomToast ref={toast} />
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    genderText: {
        fontSize: 12,
        fontFamily: Fonts.APEX_MEDIUM
    }

});

const mapStateToProps = state => {
    return {
        activeLanguage: state.languageOperation.activeLanguage
    };
};

export default connect(mapStateToProps)(EditProfileScreen);