import React, { useEffect, useRef, useState } from 'react';
import { Text, View, TouchableOpacity, Animated, ActivityIndicator, Image, StyleSheet, Linking } from 'react-native';
import { Actions } from 'react-native-router-flux'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux';

import { wait, getUserData } from '../../GlobalFunction';
import { Colors, StorageKey, Images, StoreURL, Fonts, Metrics } from '../../GlobalConfig';

import CustomToast from '../../components/CustomToast';
import languageUpdateApp from './languageUpdateApp';
import { ScrollView } from 'react-native-gesture-handler';

const UpdateAppScreen = (props) => {
	const { activeLanguage } = props
	const [isPriority, setIsPriority] = useState(false)
	const [priorityUpdate, setPriorityUpdate] = useState(props.priority)
	const [isUpdateMessage, setIsUpdateMessage] = useState(true)
	const [updateMessage, setUpdateMessage] = useState(props.message)
	const [isLoading, setIsLoading] = useState(false)
	const toast = useRef(null)

	useEffect(() => {
		if(priorityUpdate=="1"){
			setIsPriority(true)
		}
		else{
			setIsPriority(false)
		}
		console.log(updateMessage)
	}, [])

	const later = () => {
		setIsLoading(true)
		AsyncStorage.getItem(StorageKey.Language)
			.then(
				val => {
					if (val) props.changeLanguage(val)
				},
				err => console.warn(err)
			)
		wait(2000)
			.then(() => {
				getUserData()
					.then((res) => {
						if (res) Actions.master()
						else Actions.home()
						setIsLoading(false)
					})
					.catch(err => Actions.home())
			})
	}

	const changeLanguage = () => {
		setIsLoading(true)
		Actions.multiOption({
			chosenType: 'language'
		})
		setIsLoading(false)
	}

	return (
		<View style={{ flex: 1 }}>
			<View style={{ position: 'absolute', top: 40, right: 24 }}>
				<TouchableOpacity
					disabled={isLoading}
					onPress={changeLanguage}
					style={[
						Platform.OS == 'ios' && { paddingTop: 4, paddingLeft: 1 },
						{ height: 36, width: 36, justifyContent: 'center', alignItems: 'center', borderRadius: 18, borderWidth: 2, borderColor: Colors.RED, backgroundColor: Colors.WHITE }
					]}>
					<Text style={{ fontFamily: Fonts.APEX_BOLD, fontSize: 14, letterSpacing: 0.5, color: Colors.RED, textAlign: 'center' }}>{activeLanguage}</Text>
				</TouchableOpacity>
			</View>
			<View style={{ marginTop: Metrics.SAFE_AREA, flex: 1, justifyContent: 'center' }}>
				<Image resizeMode="contain" source={Images.LOGO} style={{ width: 200, height: 180, tintColor: Colors.RED, alignSelf: 'center' }} />
			</View>
			<View style={{ marginVertical: Metrics.SAFE_AREA, flex: 2, alignItems: 'center' }}>
				<View style={{ flex: 2, width: 250, height: Metrics.SCREEN_HEIGHT * 0.3, justifyContent: 'space-around' }}>
					<View style={{ flex: 1, justifyContent: 'center' }}>
						<Text style={[isUpdateMessage && { marginBottom: 20 }, { fontFamily: Fonts.APEX_BOLD, fontSize: 24, color: Colors.BLACK, textAlign: 'center' }]}>{languageUpdateApp[activeLanguage].TEXT_UPDATE_HEADER}</Text>
					</View>
					<View style={{ flex: 1, justifyContent: 'center' }}>
						{isUpdateMessage ?
							<ScrollView
								style={{ paddingBottom: Metrics.SAFE_AREA }}>
								<Text style={{ fontFamily: Fonts.APEX_REGULAR, fontSize: 20, color: Colors.BLACK, textAlign: 'center' }}>{updateMessage}</Text>
							</ScrollView>
							:
							<></>
						}
					</View>
				</View>
			</View>
			<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
				<Text style={{ fontFamily: Fonts.APEX_REGULAR, fontSize: 18, color: Colors.BLACK, textAlign: 'center', width: 250, marginBottom: 50 }}>{languageUpdateApp[activeLanguage].TEXT_UPDATE_BODY}</Text>
				<View style={{ justifyContent: "space-evenly", width: '100%', flexDirection: 'row' }}>
					{isLoading ? <ActivityIndicator color={Colors.RED} size="large" />
						: <>
							{isPriority ? <></>
								:
								<TouchableOpacity
									onPress={later}
									disabled={isLoading}
									style={(isLoading) ? { opacity: 0 } :
										{
											backgroundColor: Colors.GRAY,
											alignSelf: 'center',
											height: Metrics.SCREEN_HEIGHT * 0.08,
											width: Metrics.SCREEN_WIDTH * 0.4,
											borderRadius: 40,
											alignItems: 'center',
											justifyContent: 'center'
										}
									}>
									<Text style={{ fontFamily: Fonts.APEX_MEDIUM, color: '#fff', fontSize: 18 }}>{languageUpdateApp[activeLanguage].BUTTON_LATER}</Text>
								</TouchableOpacity>
							}
							<TouchableOpacity
								onPress={() => Linking.openURL(StoreURL)}
								disabled={isLoading}
								style={(isLoading) ? { opacity: 0 } :
									{
										backgroundColor: Colors.RED,
										alignSelf: 'center',
										height: Metrics.SCREEN_HEIGHT * 0.08,
										width: Metrics.SCREEN_WIDTH * 0.4,
										borderRadius: 40,
										alignItems: 'center',
										justifyContent: 'center'
									}
								}>
								<Text style={{ fontFamily: Fonts.APEX_MEDIUM, color: '#fff', fontSize: 18 }}>{languageUpdateApp[activeLanguage].BUTTON_UPDATE}</Text>
							</TouchableOpacity>
						</>}
				</View>
			</View>
			<CustomToast ref={toast} />
		</View>
	);
}

const styles = StyleSheet.create({
	headertext: {
		fontFamily: Fonts.APEX_MEDIUM,
		color: Colors.BLACK,
		textAlign: 'center',
		fontSize: 24
	},
	buttonBigTextScan: {
		fontFamily: Fonts.APEX_BOLD,
		color: Colors.BLACK,
		textAlign: 'center',
		fontSize: 28,
		letterSpacing: 1
	},

});

const mapStateToProps = state => {
	return {
		activeLanguage: state.languageOperation.activeLanguage
	};
};

export default connect(mapStateToProps)(UpdateAppScreen);