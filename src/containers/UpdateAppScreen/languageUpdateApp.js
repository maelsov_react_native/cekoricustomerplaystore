export default {
    EN: {
        TEXT_UPDATE_HEADER: 'Software Update Required',
        TEXT_UPDATE_BODY: 'For best user experience, please update to the latest version software',

        BUTTON_UPDATE: 'Update',
        BUTTON_LATER: 'Later',
    },
    ID: {
        TEXT_UPDATE_HEADER: 'Butuh Pembaharuan Perangkat Lunak',
        TEXT_UPDATE_BODY: 'Untuk pengalaman pengguna terbaik, silahkan perbarui perangkat lunak ke versi terbaru',

        BUTTON_UPDATE: 'Perbarui',
        BUTTON_LATER: 'Nanti',
    },
}