export default {
    EN: {
        TEXT_CONTACT_US: 'CONTACT US',
        TOUCHABLE_TERMS_CONDITION: 'Terms & Condition',
        TOUCHABLE_PRIVACY_POLICY: 'Privacy Policy',
    },
    ID: {
        TEXT_CONTACT_US: 'HUBUNGI KAMI',
        TOUCHABLE_TERMS_CONDITION: 'Syarat & Ketentuan',
        TOUCHABLE_PRIVACY_POLICY: 'Kebijakan Privasi',
    },
}