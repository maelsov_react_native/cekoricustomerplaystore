import React, { Component } from 'react';
import { View, TouchableOpacity, Linking, Image, Text, ScrollView } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Colors, Fonts, Metrics, Images } from '../../GlobalConfig'
import { connect } from 'react-redux';
import languageInfo from './languageInfo';

const InfoScreen =(props)=> {
    const { activeLanguage } = props

    const socialMediaPress = (name) => {
        switch (name) {
            case 'url':
                Linking.openURL("https://www.cekori.com")
                break;
            case 'facebook':
                Linking.openURL("https://www.facebook.com/cekori.id")
                break;
            case 'twitter':
                break;
            case 'instagram':
                Linking.openURL("https://www.instagram.com/cekori.id/")
                break;
        }
    }

   const helpMenuPress = (name) => {
        switch (name) {
            case 'termsCondition':
                Linking.openURL("http://www.cekori.id/terms-of-service")
                break;
            case 'privacyPolicy':
                Linking.openURL("http://www.cekori.id/privacy")
                break;
        }
    }

        return (
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                <ScrollView>
                    <View style={{ marginTop: Metrics.SAFE_AREA, height: 150, justifyContent: 'center' }}>
                        <Image resizeMode="contain" source={Images.LOGO} style={{ width: 200, height: 180, tintColor: Colors.RED, alignSelf: 'center' }} />
                    </View>
                    <View style={{ marginVertical: Metrics.SAFE_AREA, width: '100%', alignItems: 'center' }}>
                        <View style={{ width: 250, height: 250, justifyContent: 'space-around' }}>
                            <Text style={{ fontFamily: Fonts.APEX_BOLD, fontSize: 24, color: Colors.BLACK, textAlign: 'center' }}>{languageInfo[activeLanguage].TEXT_CONTACT_US}</Text>
                            <View style={{ alignItems: 'center', width: '100%' }}>
                                <TouchableOpacity onPress={() => socialMediaPress('url')} style={{ borderBottomWidth: 1 }}>
                                    <Text style={{ fontFamily: Fonts.APEX_REGULAR, fontSize: 20, color: Colors.BLACK }}>www.cekori.com</Text>
                                </TouchableOpacity>
                            </View>
                            <Text style={{ fontFamily: Fonts.APEX_REGULAR, fontSize: 20, color: Colors.BLACK, textAlign: 'center' }}>@cekori.id</Text>
                            <View style={{ flexDirection: 'row', height: 60, width: '100%', paddingHorizontal: Metrics.SAFE_AREA, justifyContent: 'space-around' }}>
                                <TouchableOpacity onPress={() => socialMediaPress('facebook')} style={{ height: 60, width: 60, borderRadius: 30, backgroundColor: Colors.RED, justifyContent: 'center', alignItems: 'center' }}>
                                    <FontAwesome name="facebook" color={Colors.WHITE} size={36} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => socialMediaPress('instagram')} style={{ height: 60, width: 60, borderRadius: 30, backgroundColor: Colors.RED, justifyContent: 'center', alignItems: 'center' }}>
                                    <FontAwesome name="instagram" color={Colors.WHITE} size={36} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 1, paddingLeft: Metrics.SAFE_AREA }}>
                        <TouchableOpacity onPress={() => helpMenuPress('termsCondition')} style={{ width: '100%', borderTopWidth: 1, borderTopColor: '#b7b7b7', height: 50, justifyContent: 'center' }}>
                            <Text style={{ fontFamily: Fonts.APEX_REGULAR, fontSize: 24, color: Colors.BLACK }}>{languageInfo[activeLanguage].TOUCHABLE_TERMS_CONDITION}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => helpMenuPress('privacyPolicy')} style={{ width: '100%', borderTopWidth: 1, borderBottomWidth: 1, borderTopColor: '#b7b7b7', borderBottomColor: '#b7b7b7', height: 50, justifyContent: 'center' }}>
                            <Text style={{ fontFamily: Fonts.APEX_REGULAR, fontSize: 24, color: Colors.BLACK }}>{languageInfo[activeLanguage].TOUCHABLE_PRIVACY_POLICY}</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }

const mapStateToProps = state => {
    return {
        activeLanguage: state.languageOperation.activeLanguage
    };
};

export default connect(mapStateToProps)(InfoScreen);