import React, { useEffect, useRef, useState } from 'react';
import { Text, TouchableOpacity, Platform, Image, View, ActivityIndicator, Vibration, PermissionsAndroid, Linking } from 'react-native';
import { Actions } from 'react-native-router-flux'
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoder';
import DeviceInfo from 'react-native-device-info';
import Modal from 'react-native-modal';
import { RNCamera } from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';
import Share from 'react-native-share';
import { connect } from 'react-redux';

import { Fonts, Colors, StoreURL, Metrics, Version, StorageKey } from '../../GlobalConfig';
import { getDatabaseDateTime, getUserData, wait } from '../../GlobalFunction';
import { modelQR } from '../../models/QR';
import languageQRCode from './languageQRCode';

import CustomToast from '../../components/CustomToast';
import GlobalLanguage from '../../GlobalLanguage';
import AsyncStorage from '@react-native-community/async-storage';

const LOCATION_ERROR = {
	REJECTED: 'rejected',
	NOT_FOUND: 'not_found',
	TIMEOUT: 'timeout',
}

const QRCodeScreen = (props) => {
	const { activeLanguage } = props
	const [latitude, setLatitude] = useState(0)
	const [longitude, setLongitude] = useState(0)
	const [country, setCountry] = useState(null)
	const [province, setProvince] = useState(null)
	const [regency, setRegency] = useState(null)
	const [district, setDistrict] = useState(null)
	const [address, setAddress] = useState(null)
	const [dataUser, setDataUser] = useState([])
	const [locationErrorStatus, setLocationErrorStatus] = useState("")
	const [kodeProduct1, setKodeProduct1] = useState("")
	const [isModalProductInformation, setIsModalProductInformation] = useState(false)
	const [isModalVisible, setIsModalVisible] = useState(false)
	const [isModalProductTidakTerdaftar, setIsModalProductTidakTerdaftar] = useState(false)
	const [isLoading, setIsLoading] = useState(false)
	const [isLoadingLocation, setIsLoadingLocation] = useState(false)
	const [isAllowScan, setIsAllowScan] = useState(true)
	const [permissionStatus, setPermissionStatus] = useState("granted")
	const [isModalScanAgain, setIsModalScanAgain] = useState(false)
	const [isEnableScan, setIsEnableScan] = useState(true)
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [timeStamp, setTimeStamp] = useState(getDatabaseDateTime)
	const [originalMessage, setOriginalMessage] = useState("")
	const [imageOriginal, setImageOriginal] = useState("")
	const [QRType, setQRType] = useState("")
	const [isAllowCamera, setIsAllowCamera] = useState(false)
	const toast = useRef(null)

	useEffect(() => {
		wait(0).then(() => Actions.refresh({ title: languageQRCode[activeLanguage].TEXT_HEADER }))
		getUserData().then(res => {
			setDataUser(res)
		})
		wait(500).then(checkPermission())
		getGPSLocation()
	}, [])

	useEffect(() => {
		AsyncStorage.getItem(StorageKey.userFirstName, (err, res) => {
			if (res) {
				setFirstName(JSON.parse(res))
			}
		})
		AsyncStorage.getItem(StorageKey.userLastName, (err, res) => {
			if (res) {
				setLastName(JSON.parse(res))
			}
		})

	}, [])

	const checkPermission = () => {
		if (Platform.OS == 'android') {
			PermissionsAndroid.requestMultiple([
				PermissionsAndroid.PERMISSIONS.CAMERA,
				PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
			])
				.then(res => {
					if (Object.values(res).every((result) => result === 'granted')) {
						setPermissionStatus('granted')
						getGPSLocation()
						setIsAllowCamera(true)
					}
					else if (Object.values(res).some((result) => result === 'never_ask_again')) {
						setPermissionStatus('never_ask_again')
					}
					else {
						setPermissionStatus('denied')
					}
				})
		}
	}

	const openSetting = () => {
		Linking.openSettings()
		wait(1000).then(() => setPermissionStatus('denied'))
	}

	const scanQr2 = () => {
		setIsModalScanAgain(false)
		setIsAllowScan(false)
		setIsEnableScan(true)
	}

	const checkOldQR = (data) => {
		if (kodeProduct1 == '') {
			setKodeProduct1(data)
			setIsLoading(true)
			if (isAllowScan) {
				const formdata = new FormData();
				formdata.append('user_id', dataUser.user_id);
				formdata.append('transaction_imei', DeviceInfo.getUniqueId());
				formdata.append('transaction_product_barcode', data);
				formdata.append('transaction_lat', latitude);
				formdata.append('transaction_long', longitude);
				formdata.append('transaction_country', country);
				formdata.append('transaction_province', province);
				formdata.append('transaction_regency', regency);
				formdata.append('transaction_district', district);
				formdata.append('transaction_address', address);

				console.log(formdata)

				modelQR.scanQR(formdata, res => {
					const { status, result } = res
					setIsLoading(false)
					console.log(res)
					switch (status) {
						case 200:
							setImageOriginal(result.message.product_image)
							setIsModalProductInformation(true)
							setIsModalVisible(true)
							setIsLoading(false)
							break;
						case 404:
							setKodeProduct1("")
							toast.current.ShowToastFunction("error", languageQRCode[activeLanguage].TOAST_ERROR_QR_INVALID)
							wait(2000).then(() => setIsEnableScan(true))
							setIsLoading(false)
							setIsAllowCamera(true)
							break;
						case 400:
						default:
							toast.current.ShowToastFunction("error", languageQRCode[activeLanguage].TOAST_ERROR_QR_INVALID)
							setKodeProduct1("")
							wait(2000).then(() => setIsEnableScan(true))
							setIsLoading(false)
							setIsAllowCamera(true)
							break;
					}
				})
					.catch((error) => {
						console.log(error.message);
						toast.current.ShowToastFunction("error", GlobalLanguage[activeLanguage].TOAST_ERROR_DEFAULT)
					})
			}
		}
	}

	const successScanProduct = ({ data }) => {
		if (isAllowScan) {
			checkOldQR(data)
		}
	}

	// 	const getGPSLocation = () => {
	// 		setPermissionStatus('granted')
	// 		setIsLoading(true)
	// 		setIsLoadingLocation(true)
	// 		Geolocation.getCurrentPosition(info => {
	// 			setLatitude(info.coords.latitude)
	// 			setLongitude(info.coords.longitude)
	// 			setIsLoadingLocation(false)
	// 			setIsLoading(false)
	// 		});
	// 		Geocoder.from(latitude, longitude)
	// 			.then(json => {
	// 				console.log(json);
	// 				var addressComponent = json.results[0].address_components;
	// 				setAddress(addressComponent)
	// 				console.log(addressComponent);
	// 			}).catch(error => console.warn(error));
	// }

	getGPSLocation = () => {
		setPermissionStatus('granted')
		setIsLoading(true)
		setIsLoadingLocation(true)
		Geolocation.getCurrentPosition(
			(position) => {
				const { latitude, longitude } = position.coords
				setLatitude(latitude)
				setLongitude(longitude)
				Geocoder.fallbackToGoogle('AIzaSyCjoRDkS-V40g4kPrD9GdowldisOdhSSLQ');
				Geocoder.geocodePosition({ lat: latitude, lng: longitude })
					// Geocoder.geocodePosition({ lat: -6.1906267, lng: 106.8702902 })
					.then(res => {
						const { country, adminArea, subAdminArea, formattedAddress } = res[0]
						const district = formattedAddress.split(',')[3]
						setLatitude
						setLongitude
						setCountry(country)
						setDistrict(district)
						setProvince(adminArea)
						setRegency(subAdminArea)
						setAddress(formattedAddress)
						setIsLoadingLocation(false)
						setIsLoading(false)
						setLocationErrorStatus('')
					})
					.catch(err => {
						setIsLoadingLocation(false)
						setLocationErrorStatus(LOCATION_ERROR.NOT_FOUND)
					})
			},
			(error) => {
				console.warn(error.code)
				switch (error.code) {
					case 1:
						setIsLoadingLocation(false)
						setLocationErrorStatus(LOCATION_ERROR.REJECTED)
						if (Platform.OS == 'ios') {
							setPermissionStatus('never_ask_again')
						}
						break;
					case 2:
						setIsLoadingLocation(false)
						setLocationErrorStatus(LOCATION_ERROR.NOT_FOUND)
						break;
					case 3:
						setIsLoadingLocation(false)
						setLocationErrorStatus(LOCATION_ERROR.TIMEOUT)
						break;
				}
			},
			{
				maximumAge: 10000,
				timeout: 30000
			}
		);
	}


	const _hideModal = () => {
		setIsModalVisible(false)
		setIsModalProductInformation(false)
		setIsModalProductTidakTerdaftar(false)
	}

	const finishSeeProducts = () => {
		_hideModal()
		Actions.pop()
	}

	const share = () => {
		const options = {
			message: GlobalLanguage[activeLanguage].TEXT_CHECK_PRODUCT,
			url: StoreURL
		};
		Share.open(options)
			.then((res) => { console.log(res) })
			.catch((err) => { console.log(err) })
	}


	var modalContent = <View />

	if (isModalProductTidakTerdaftar) {
		modalContent = (
			<View style={{ backgroundColor: "#fff", padding: 15, borderRadius: 20, width: '100%', maxHeight: Metrics.SCREEN_WIDTH * 0.7, justifyContent: 'space-evenly', alignItems: 'center' }}>
				<Text style={{ textAlign: "center", color: Colors.BLACK, fontFamily: Fonts.APEX_REGULAR, fontSize: 20, fontSize: 16 }}>{languageQRCode[activeLanguage].TEXT_MODAL_QR_RESULT_BODY_3}</Text>
				<Text style={{ textAlign: "center", color: Colors.RED, fontFamily: Fonts.APEX_BOLD, fontSize: 24, letterSpacing: 1 }}>{languageQRCode[activeLanguage].TEXT_MODAL_QR_RESULT_DOUBTED}</Text>
				<Text style={{ textAlign: "center", color: Colors.BLACK, fontFamily: Fonts.APEX_MEDIUM, fontSize: 20, marginVertical: 30 }}>{languageQRCode[activeLanguage].TEXT_MODAL_QR_ERROR_BODY}</Text>
				<View style={{ justifyContent: "space-evenly", width: '100%' }}>
					<TouchableOpacity
						onPress={_hideModal}
						style={{
							backgroundColor: Colors.RED,
							alignSelf: 'center',
							borderRadius: 40,
							height: Metrics.SCREEN_HEIGHT * 0.07,
							width: 100,
							alignItems: 'center',
							justifyContent: 'center'
						}}>
						<Text style={{ fontFamily: Fonts.APEX_MEDIUM, color: '#fff', fontSize: 18 }}>{languageQRCode[activeLanguage].BUTTON_MODAL_OK}</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
	}

	if (isModalScanAgain) {
		modalContent = (
			<View style={{ backgroundColor: "#fff", padding: 15, borderRadius: 20, width: '100%', height: Metrics.SCREEN_WIDTH * 0.7, justifyContent: 'space-evenly', alignItems: 'center' }}>
				<Text style={{ textAlign: "center", color: Colors.BLACK, fontFamily: Fonts.APEX_MEDIUM, fontSize: 20, lineHeight: 24 }}>{languageQRCode[activeLanguage].TOAST_SCANNED_STATUS}</Text>
				<Text style={{ textAlign: "center", color: Colors.BLACK, fontFamily: Fonts.APEX_MEDIUM, fontSize: 20 }}>{languageQRCode[activeLanguage].TOAST_INSTRUCTION}</Text>
				<View style={{ justifyContent: "space-evenly", width: '100%', flexDirection: 'row' }}>
					<TouchableOpacity
						onPress={scanQr2}
						style={{
							backgroundColor: Colors.RED,
							alignSelf: 'center',
							borderRadius: 40,
							height: Metrics.SCREEN_HEIGHT * 0.07,
							width: 100,
							alignItems: 'center',
							justifyContent: 'center'
						}}>
						<Text style={{ fontFamily: Fonts.APEX_MEDIUM, color: '#fff', fontSize: 18 }}>{languageQRCode[activeLanguage].BUTTON_SCAN}</Text>
					</TouchableOpacity>
				</View>
			</View >
		)
	}

	if (isModalProductInformation) {
		modalContent = (
			<View style={{ backgroundColor: "#fff", padding: 15, borderRadius: 20, width: '100%', maxHeight: Metrics.SCREEN_WIDTH - 40 }}>
				{originalMessage == "" ?
					(<>
						<Text style={{ textAlign: "center", color: Colors.BLACK, fontFamily: Fonts.APEX_MEDIUM, fontSize: 20, lineHeight: 24 }}>{languageQRCode[activeLanguage].TEXT_MODAL_QR_RESULT_BODY_1}</Text>
						<Text style={{ textAlign: "center", color: Colors.BLACK, fontFamily: Fonts.APEX_MEDIUM, fontSize: 20 }}>{languageQRCode[activeLanguage].TEXT_MODAL_QR_RESULT_BODY_2}</Text>
						<View style={{ alignSelf: 'center', justifyContent: 'center', marginVertical: 15 }}>
							<Image style={{ width: 100, height: 100 }} resizeMode='contain' source={{ uri: imageOriginal }} />
						</View>
					</>)
					:
					(<>
						<Text style={{ textAlign: "center", color: Colors.BLACK, fontFamily: Fonts.APEX_REGULAR, fontSize: 20, lineHeight: 24, marginVertical: 5 }}>{languageQRCode[activeLanguage].TEXT_NEW_SCANNED_STATUS_1}</Text>
						<Text style={{ textAlign: "center", color: Colors.BLACK, fontFamily: Fonts.APEX_MEDIUM, fontSize: 20, lineHeight: 24, marginVertical: 5 }}>{originalMessage}</Text>
						<Text style={{ textAlign: "center", color: Colors.BLACK, fontFamily: Fonts.APEX_REGULAR, fontSize: 20, lineHeight: 24, marginBottom: 25 }}>{languageQRCode[activeLanguage].TEXT_NEW_SCANNED_STATUS_2}</Text>
					</>)
				}
				<Text style={{ textAlign: "center", color: Colors.BLACK, fontFamily: Fonts.APEX_REGULAR, fontSize: 20, fontSize: 16 }}>{languageQRCode[activeLanguage].TEXT_MODAL_QR_RESULT_BODY_3}</Text>
				<Text style={{ textAlign: "center", color: Colors.GREEN, fontFamily: Fonts.APEX_BOLD, marginBottom: 10, fontSize: 24, letterSpacing: 1 }}>{languageQRCode[activeLanguage].TEXT_MODAL_QR_RESULT_ORIGINAL}</Text>
				<View style={{ justifyContent: "space-evenly", width: '100%', flexDirection: 'row' }}>
					<TouchableOpacity
						onPress={share}
						style={{
							backgroundColor: Colors.RED,
							alignSelf: 'center',
							borderRadius: 40,
							height: Metrics.SCREEN_HEIGHT * 0.07,
							width: 100,
							alignItems: 'center',
							justifyContent: 'center'
						}}>
						<Text style={{ fontFamily: Fonts.APEX_MEDIUM, color: '#fff', fontSize: 18 }}>{languageQRCode[activeLanguage].BUTTON_MODAL_SHARE}</Text>
					</TouchableOpacity>

					<TouchableOpacity
						onPress={finishSeeProducts}
						style={{
							backgroundColor: Colors.RED,
							alignSelf: 'center',
							borderRadius: 40,
							height: Metrics.SCREEN_HEIGHT * 0.07,
							width: 100,
							alignItems: 'center',
							justifyContent: 'center'
						}}>
						<Text style={{ fontFamily: Fonts.APEX_MEDIUM, color: '#fff', fontSize: 18 }}>{languageQRCode[activeLanguage].BUTTON_MODAL_OK}</Text>
					</TouchableOpacity>
				</View>
			</View >
		)
	}

	let permissionBody = languageQRCode[activeLanguage].TEXT_PERMISSION_BODY_DENIED
	let permissionButtonText = languageQRCode[activeLanguage].BUTTON_TRY_AGAIN
	let permissionButtonFunction = checkPermission

	if (permissionStatus == 'never_ask_again') {
		permissionBody = languageQRCode[activeLanguage].TEXT_PERMISSION_BODY_NEVER_ASK
		permissionButtonText = languageQRCode[activeLanguage].BUTTON_OPEN_SETTING
		permissionButtonFunction = openSetting
	}

	if (permissionStatus != 'granted') {
		return (
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<View style={{ width: '80%' }}>
					<Text style={{ fontSize: 20, fontFamily: Fonts.APEX_MEDIUM, color: Colors.BLACK, textAlign: 'center', lineHeight: 32 }}>{permissionBody}</Text>
				</View>
				<TouchableOpacity style={{ marginTop: 50, marginBottom: 20, paddingVertical: 8, paddingHorizontal: 10, backgroundColor: Colors.RED, borderRadius: 8 }} onPress={permissionButtonFunction}>
					<Text style={{ fontFamily: Fonts.APEX_MEDIUM, fontSize: 16, color: Colors.WHITE, letterSpacing: 0.5 }}>{permissionButtonText}</Text>
				</TouchableOpacity>
			</View>
		)
	}

	let viewcontent = (
		<>
			{isAllowCamera &&
				<RNCamera
					style={{ height: Metrics.SCREEN_WIDTH, width: Metrics.SCREEN_WIDTH, overflow: 'hidden' }}
					captureAudio={false}
					onBarCodeRead={successScanProduct}
					barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
				>
					<BarcodeMask
						width={200}
						height={200}
						edgeWidth={20}
						edgeColor={Colors.RED_DARK}
						showAnimatedLine
						animatedLineColor={Colors.RED}
						lineAnimationDuration={3000}
						edgeBorderWidth={4} />
				</RNCamera>
			}
		</>
	)

	if (isLoading || isLoadingLocation) {
		viewcontent = (
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<ActivityIndicator color={Colors.RED} size='large' />
				{isLoadingLocation && (
					<View style={{ marginTop: 20 }}>
						<Text style={{ fontSize: 20, fontFamily: Fonts.APEX_MEDIUM, color: Colors.BLACK, textAlign: 'center', lineHeight: 32 }}>{languageQRCode[activeLanguage].TEXT_LOADING_LOCATION}</Text>
					</View>
				)}
			</View>
		)
	}

	let locationErrorBody = languageQRCode[activeLanguage].TEXT_LOCATION_BODY_REJECTED
	let locationErrorButtonText = languageQRCode[activeLanguage].BUTTON_TRY_AGAIN
	let locationErrorButtonFunction = getGPSLocation

	switch (locationErrorStatus) {
		case LOCATION_ERROR.NOT_FOUND:
			locationErrorBody = languageQRCode[activeLanguage].TEXT_LOCATION_BODY_NOT_FOUND
			break;
		case LOCATION_ERROR.TIMEOUT:
			locationErrorBody = languageQRCode[activeLanguage].TEXT_LOCATION_BODY_TIMEOUT
			break;
		default:
			break;
	}

	if (locationErrorStatus != '') {
		return (
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<View style={{ width: '80%' }}>
					<Text style={{ fontSize: 20, fontFamily: Fonts.APEX_MEDIUM, color: Colors.BLACK, textAlign: 'center', lineHeight: 32 }}>{locationErrorBody}</Text>
				</View>
				<TouchableOpacity style={{ marginTop: 50, marginBottom: 20, paddingVertical: 8, paddingHorizontal: 10, backgroundColor: Colors.RED, borderRadius: 8 }} onPress={locationErrorButtonFunction}>
					<Text style={{ fontFamily: Fonts.APEX_MEDIUM, fontSize: 16, color: Colors.WHITE, letterSpacing: 0.5 }}>{locationErrorButtonText}</Text>
				</TouchableOpacity>
			</View>
		)
	}

	return (
		<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
			{viewcontent}
			<Modal
				onBackButtonPress={_hideModal}
				onBackdropPress={_hideModal}
				children={modalContent}
				avoidKeyboard={Platform.OS === "ios"}
				isVisible={isModalVisible || isModalScanAgain || isModalProductTidakTerdaftar}
				backdropOpacity={0.4} />

			<CustomToast ref={toast} />
		</View >
	);

}
const mapStateToProps = state => {
	return {
		activeLanguage: state.languageOperation.activeLanguage
	};
};

export default connect(mapStateToProps)(QRCodeScreen);