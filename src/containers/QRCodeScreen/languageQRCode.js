export default {
    EN: {
        TEXT_HEADER: 'HOLD OVER A QR CODE',

        TEXT_PERMISSION_BODY_DENIED: 'Please allow camera and location access to use this feature',
        TEXT_PERMISSION_BODY_NEVER_ASK: 'Please allow camera and location access from setting to use this feature',
        TEXT_LOADING_LOCATION: 'Loading Location',
        TEXT_LOCATION_BODY_REJECTED: 'Please allow camera and location access to use this feature',
        TEXT_LOCATION_BODY_NOT_FOUND: 'Please allow camera and location access from setting to use this feature',
        TEXT_LOCATION_BODY_TIMEOUT: 'Please allow camera and location access from setting to use this feature',

        BUTTON_TRY_AGAIN: 'Try Again',
        BUTTON_OPEN_SETTING: 'Open Setting',
        BUTTON_MODAL_OK: 'OK',
        BUTTON_MODAL_SHARE: 'SHARE',
        BUTTON_SCAN: 'Scan',

        TEXT_MODAL_QR_ERROR_BODY: 'Product with this QR code is not found',
        TEXT_MODAL_QR_RESULT_BODY_1: 'THANK YOU',
        TEXT_MODAL_QR_RESULT_BODY_2: 'FOR SCANNING',
        TEXT_MODAL_QR_RESULT_BODY_3: 'THIS PRODUCT',
        TEXT_MODAL_QR_RESULT_ORIGINAL: 'ORIGINAL',
        TEXT_MODAL_QR_RESULT_DOUBTED: 'DOUBTED',
        TEXT_QR_NOT_FOUND: 'QR Code Not Found',
        TEXT_NEW_SCANNED_STATUS_1: 'You are buyer number :',
        TEXT_NEW_SCANNED_STATUS_2: 'who performed a scan of this product',

        TOAST_QR_ALREADY_SCANNED : 'This QR is already scanned',
        TOAST_SCANNED_STATUS : 'QR Scanned',
        TOAST_INSTRUCTION : 'Please scan another QR code which are available on the product',
        TOAST_ERROR_QR_INVALID: 'Invalid QR Code',

        TOAST_ERROR_ER01_OLD: 'Invalid QR Code',
        TOAST_ERROR_ER02_NEW: 'Invalid QR Code',
        TOAST_ERROR_ER10_NEW: 'This Brand Is No Longer Partner Of Cek Ori',
        
        TOAST_ERROR_ER01_NEW: 'Invalid Form Request',
    },
    ID: {
        TEXT_HEADER: 'ARAHKAN KE QR CODE',

        TEXT_PERMISSION_BODY_DENIED: 'Mohon izinkan akses kamera dan lokasi untuk menggunakan fitur ini',
        TEXT_PERMISSION_BODY_NEVER_ASK: 'Mohon izinkan akses kamera dan lokasi dari pengaturan untuk menggunakan fitur ini',
        TEXT_LOADING_LOCATION: 'Memuat Lokasi',
        TEXT_LOCATION_BODY_REJECTED: 'Izin lokasi ditolak, mohon aktifkan dari pengaturan HP',
        TEXT_LOCATION_BODY_NOT_FOUND: 'Lokasi tidak ditemukan',
        TEXT_LOCATION_BODY_TIMEOUT: 'Pencarian lokasi melebihi batas waktu',

        BUTTON_TRY_AGAIN: 'Coba lagi',
        BUTTON_OPEN_SETTING: 'Buka Pengaturan',
        BUTTON_MODAL_OK: 'OK',
        BUTTON_MODAL_SHARE: 'BAGIKAN',
        BUTTON_SCAN: 'Pindai',

        TEXT_MODAL_QR_ERROR_BODY: 'Product dengan kode QR ini tidak ditemukan',
        TEXT_MODAL_QR_RESULT_BODY_1: 'TERIMA KASIH',
        TEXT_MODAL_QR_RESULT_BODY_2: 'TELAH MEMINDAI',
        TEXT_MODAL_QR_RESULT_BODY_3: 'PRODUK INI',
        TEXT_MODAL_QR_RESULT_ORIGINAL: 'ORIGINAL',
        TEXT_MODAL_QR_RESULT_DOUBTED: 'DIRAGUKAN',
        TEXT_QR_NOT_FOUND: 'Kode QR Tidak Ditemukan',
        TEXT_NEW_SCANNED_STATUS_1: 'Anda adalah pembeli ke :',
        TEXT_NEW_SCANNED_STATUS_2: 'yang telah melakukan pemindaian produk ini',

        TOAST_QR_ALREADY_SCANNED : 'Kode QR ini telah dipindai',
        TOAST_SCANNED_STATUS : 'Kode QR telah dipindai',
        TOAST_INSTRUCTION : 'Silahkan pindai kode QR lain yang ada pada produk',
        TOAST_ERROR_QR_INVALID: 'Kode QR Tidak Valid',

        TOAST_ERROR_ER01_OLD: 'Kode QR Tidak Valid',
        TOAST_ERROR_ER02_NEW: 'Kode QR Tidak Valid',
        TOAST_ERROR_ER10_NEW: 'Merk Ini Bukan Bagian Dari Cek Ori Lagi',
        
        TOAST_ERROR_ER01_NEW: 'Bentuk Pesan Tidak Valid',
    },
}