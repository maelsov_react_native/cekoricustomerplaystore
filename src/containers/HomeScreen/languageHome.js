export default {
    EN: {
        TEXT_BACKGROUND_TITLE: 'Welcome to',
        TEXT_BACKGROUND_BODY: 'Enjoy the convenience of systems that guarantee product authenticity',
        
        TEXT_PASSWORD_HEADER: 'Password must at least include',


        TEXT_MODAL_LOGIN_HEADER: 'Login',
        TEXT_MODAL_FORGOT_PASSWORD_HEADER: 'Forgot Password',
        TEXT_MODAL_REGISTER_HEADER: 'Register',

        TEXT_MODAL_FORGOT_PASSWORD_BODY_1: 'Enter the email address used during registration',
        TEXT_MODAL_FORGOT_PASSWORD_BODY_2: 'An email containing instructions on password change will be sent to the registered email address',

        TEXT_MODAL_REGISTER_BODY_1: 'I have agreed with',
        TEXT_MODAL_REGISTER_BODY_2: 'and',
        TEXT_MODAL_REGISTER_BODY_3: 'on cekori.com',

        TEXT_VALIDATION_LOWERCASE : '1 lower case letter',
        TEXT_VALIDATION_UPPERCASE : '1 upper case letter',
        TEXT_VALIDATION_ONEDIGIT : '1 digit number',
        TEXT_VALIDATION_ABOVE8CHAR : 'above 8 character',
        TEXT_VALIDATION_SYMBOL : '1 special character (! * ? _ . - @ #)',

        TOUCHABLE_TERMS_CONDITION: 'Terms & Condition',
        TOUCHABLE_PRIVACY_POLICY: 'Privacy Policy',

        TOUCHABLE_FORGOT_PASSWORD: 'Forgot Password?',

        BUTTON_LOGIN: 'Login',
        BUTTON_REGISTER: 'Register',
        BUTTON_SEND_EMAIL: 'Send Email',
        BUTTON_CREATE_ACCOUNT: 'Create Account',

        TOAST_WARNING_ACCOUNT_NOT_ACTIVE_1: 'Account not activated, please check your email for account verification',
        TOAST_WARNING_ACCOUNT_NOT_ACTIVE_2: '(Spam folder included)',
        TOAST_WARNING_ACCOUNT_NOT_REGISTERED: 'Account not registered',
        TOAST_WARNING_LOGIN_ERROR: 'Incorrect Phone number or email',
        TOAST_WARNING_UNCHECKED_PRIVACY: 'You have not agree with terms and condition and privacy policy',

        TOAST_WARNING_DUPLICATE_NUMBER: 'Your phone number already registered',
        TOAST_WARNING_EMAIL_NOT_REGISTERED: 'Email not registered',

        TOAST_SUCCESS_LOGIN: 'Login success',
        TOAST_SUCCESS_REGISTER_1: 'Register success, welcome',
        TOAST_SUCCESS_REGISTER_2: 'Please check your email to verify your account',

        TOAST_SUCCESS_FORGOT_PASSWORD_1: 'Password change email has been sent',
        TOAST_SUCCESS_FORGOT_PASSWORD_2: 'Please check your email to proceed'
    },
    ID: {
        TEXT_BACKGROUND_TITLE: 'Selamat Datang di',
        TEXT_BACKGROUND_BODY: 'Nikmati kemudahan sistem yang mampu menjamin keaslian produk',

        TEXT_PASSWORD_HEADER: 'Kata sandi harus memiliki setidaknya',

        TEXT_MODAL_LOGIN_HEADER: 'Masuk',
        TEXT_MODAL_FORGOT_PASSWORD_HEADER: 'Lupa Kata Sandi',
        TEXT_MODAL_REGISTER_HEADER: 'Daftar',

        TEXT_MODAL_FORGOT_PASSWORD_BODY_1: 'Masukkan alamat email yang digunakan saat pendaftaran',
        TEXT_MODAL_FORGOT_PASSWORD_BODY_2: 'Email berisikan instruksi pengubahan kata sandi akan dikirimkan pada alamat email yang telah terdaftar',

        TEXT_MODAL_REGISTER_BODY_1: 'Saya telah menyetujui',
        TEXT_MODAL_REGISTER_BODY_2: 'dan',
        TEXT_MODAL_REGISTER_BODY_3: 'dari cekori.com',

        TEXT_VALIDATION_LOWERCASE : '1 huruf non-kapital',
        TEXT_VALIDATION_UPPERCASE : '1 huruf kapital',
        TEXT_VALIDATION_ONEDIGIT : '1 angka',
        TEXT_VALIDATION_ABOVE8CHAR : 'diatas 8 karakter',
        TEXT_VALIDATION_SYMBOL : '1 karakter spesial (! * ? _ . - @ #)',

        TOUCHABLE_TERMS_CONDITION: 'Syarat & Ketentuan',
        TOUCHABLE_PRIVACY_POLICY: 'Kebijakan Privasi',

        TOUCHABLE_FORGOT_PASSWORD: 'Lupa Kata Sandi?',

        BUTTON_LOGIN: 'Masuk',
        BUTTON_REGISTER: 'Daftar',
        BUTTON_SEND_EMAIL: 'Kirim Email',
        BUTTON_CREATE_ACCOUNT: 'Buat Akun',

        TOAST_WARNING_ACCOUNT_NOT_ACTIVE_1: 'Akun belum aktif, silahkan cek email anda untuk verifikasi akun',
        TOAST_WARNING_ACCOUNT_NOT_ACTIVE_2: '(Termasuk folder Spam)',
        TOAST_WARNING_ACCOUNT_NOT_REGISTERED: 'Akun belum terdaftar',
        TOAST_WARNING_LOGIN_ERROR: 'Nomor telepon atau kata sandi salah',
        TOAST_WARNING_UNCHECKED_PRIVACY: 'Anda belum menyetujui syarat dan ketentuan dan kebijakan privasi',

        TOAST_WARNING_DUPLICATE_NUMBER: 'Nomor telepon anda telah terdaftar',
        TOAST_WARNING_EMAIL_NOT_REGISTERED: 'Email belum terdaftar',

        TOAST_SUCCESS_LOGIN: 'Login berhasil',
        TOAST_SUCCESS_REGISTER_1: 'Registrasi berhasil, Selamat bergabung',
        TOAST_SUCCESS_REGISTER_2: 'Silahkan cek email anda untuk verifikasi akun',

        TOAST_SUCCESS_FORGOT_PASSWORD_1: 'Email perubahan kata sandi telah dikirimkan',
        TOAST_SUCCESS_FORGOT_PASSWORD_2: 'Silahkan cek email anda untuk melanjutkan'
    },
}