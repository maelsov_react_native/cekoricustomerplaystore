import React, { useEffect, useRef, useState } from "react";
import {
    ImageBackground,
    View,
    TouchableOpacity,
    ScrollView,
    Linking,
    Text,
    ActivityIndicator,
    Platform,
    StyleSheet
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { Actions } from 'react-native-router-flux'
import Modal from 'react-native-modal'
import Swiper from 'react-native-swiper'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from "react-redux";
import { wait, isEmail, isStrongPassword, getUserData } from "../../GlobalFunction";
import { Fonts, Colors, StorageKey, Images, Metrics } from "../../GlobalConfig";
import { modelUser } from "../../models/User";
import { modelAuth } from "../../models/Auth";
import languageHome from "./languageHome";
import GlobalLanguage from "../../GlobalLanguage";
import FloatingLabelInput from "../../components/FloatingLabelInput";
import CustomToast from "../../components/CustomToast";

const HomeScreen = (props) => {
    const { activeLanguage } = props
    const [isLoading, setIsLoading] = useState(false)
    const [isModalVisible, setIsModalVisible] = useState(false)
    const [isChecked, setIsChecked] = useState(false)
    const [isModalLogin, setIsModalLogin] = useState(false)
    const [isModalRegister, setIsModalRegister] = useState(false)
    const [user_phone, setUserPhone] = useState('')
    const [user_password, setUserPassword] = useState('')
    const [user_password_confirmation, setUserPasswordConfirmation] = useState('')
    const [user_first_name, setUserFirstName] = useState('')
    const [user_last_name, setUserLastName] = useState('')
    const [user_email, setUserEmail] = useState('')
    const [user_fcm_tokern, setUserFcmToken] = useState('')
    const [activePrefix, setActivePrefix] = useState('+62')
    const [isVisibleArrow, setIsVisibleArrow] = useState(true)
    const [field, setField] = useState('')
    const [isOneLowerCase, setIsOneLowerCase] = useState(false)
    const [isOneUpperCase, setIsOneUpperCase] = useState(false)
    const [isOneDigit, setIsOneDigit] = useState(false)
    const [isOneSymbol, setIsOneSymbol] = useState(false)
    const [isAbove8Char, setIsAbove8Char] = useState(false)
    const toast = useRef(null)

    useEffect(() => {
        if (props.lastUpdate) {
            if (isModalLogin || isModalRegister) {
                wait(0).then(() => {
                    setIsModalVisible(true)
                })
            }
        }
    }, [props.lastUpdate])

    useEffect(() => {
        if (props.lastUpdatePhonePrefix) {
            setActivePrefix(props.activePrefix)
            setIsModalVisible(true)
        }
    }, [props.lastUpdatePhonePrefix])

    const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };

    const onSwiperIndexChange = () => {
        setUserPhone('')
        setUserEmail('')
        setUserPassword('')
    }

    const changeSwiperIndex = index => swiper.scrollBy(index)

    const _hideModal = () => {
        setIsModalVisible(false)
        setIsModalLogin(false)
        setIsModalRegister(false)
        setIsAbove8Char(false)
        setIsOneUpperCase(false)
        setIsOneLowerCase(false)
        setIsOneDigit(false)
        setIsOneSymbol(false)
        setUserFirstName('')
        setUserLastName('')
        setUserEmail('')
        setUserPhone('')
        setUserPassword('')
        setActivePrefix('+62')
        setIsVisibleArrow(true)
        setUserPasswordConfirmation('')
        setIsChecked(false)
    }

    const openLoginModal = () => {
        setIsModalVisible(true)
        setIsModalLogin(true)
    }

    const openRegisterModal = () => {
        setIsModalVisible(true)
        setIsModalRegister(true)
    }


    const handleLogin = () => {
        setIsLoading(true)
        if (!user_phone) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_EMPTY_PHONE_NUMBER)
            setIsLoading(false)
        }
        else if (!user_password) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_EMPTY_PASSWORD)
            setIsLoading(false)
        }
        else {
            const formdata = new FormData();
            formdata.append('user_phone', user_phone[0] === '0' ? `${user_phone.substr(1, user_phone.length - 1)}` : user_phone)
            formdata.append('user_country_phone', activePrefix)
            formdata.append('password', user_password)
            formdata.append('user_fcm_token', '-')

            console.log(formdata)

            modelAuth.login(formdata, res => {
                const { status, result } = res
                console.log(res)
                switch (status) {
                    case 200:
                        const user_data = {
                            user_token: result.message.user_token,
                            user_id: result.message.user_id,
                            user_phone: result.message.user_phone
                        }
                        AsyncStorage.setItem(StorageKey.UserData, JSON.stringify(user_data), () => {
                            toast.current.ShowToastFunction("success", languageHome[activeLanguage].TOAST_SUCCESS_LOGIN)
                            _hideModal()
                            wait(1500).then(() => {
                                setIsLoading(false)
                                Actions.master()
                            })
                        })
                        break;
                    case 401:
                        toast.current.ShowToastFunction("warning", `${languageHome[activeLanguage].TOAST_WARNING_ACCOUNT_NOT_ACTIVE_1}\n${languageHome[activeLanguage].TOAST_WARNING_ACCOUNT_NOT_ACTIVE_2}`)
                        break;
                    case 404:
                        toast.current.ShowToastFunction("warning", languageHome[activeLanguage].TOAST_WARNING_ACCOUNT_NOT_REGISTERED)
                        break;
                    case 400:
                    default:
                        toast.current.ShowToastFunction("warning", languageHome[activeLanguage].TOAST_WARNING_LOGIN_ERROR)
                        break;
                }
                if (status != 200) setIsLoading(false)
            })
                .catch((err) => {
                    console.log(err)
                    toast.current.ShowToastFunction("error", GlobalLanguage[activeLanguage].TOAST_ERROR_DEFAULT)
                    setIsLoading(false)
                })
        }
    }

    const onTextChangePassword = (value) => {
        let regLower = /[a-z]/
        let regUpper = /[A-Z]/
        let regDigit = /[0-9]/
        // let regSymbol = /[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/#@]/
        // let regSymbol = /^[a-zA-Z0-9] + [!*?_.-@#]/
        let regSymbol = /[-!:?.@#*]/
        // (! * ? _ . - @#)

        if (regLower.test(value)) setIsOneLowerCase(true)
        else setIsOneLowerCase(false)
        if (regUpper.test(value)) setIsOneUpperCase(true)
        else setIsOneUpperCase(false)
        if (regDigit.test(value)) setIsOneDigit(true)
        else setIsOneDigit(false)
        if (regSymbol.test(value)) setIsOneSymbol(true)
        else setIsOneSymbol(false)
        if (value.length > 8) setIsAbove8Char(true)
        else setIsAbove8Char(false)

        setUserPassword(value)
    }


    const handleRegister = () => {

        if (!user_first_name) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_EMPTY_FIRST_NAME)
        }
        else if (!user_phone) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_EMPTY_PHONE_NUMBER)
        }
        else if (!user_email) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_EMPTY_EMAIL)
        }
        else if (!isEmail(user_email)) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_NOT_VALID_EMAIL)
        }
        else if (!user_password) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_EMPTY_PASSWORD)
        }
        else if (!isStrongPassword(user_password)) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_NOT_VALID_PASSWORD)
        }
        else if (!user_password_confirmation) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_EMPTY_PASSWORD_CONFIRMATION)
        }
        else if (user_password != user_password_confirmation) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_DIFFERENT_PASSWORD)
        }
        else if (!isChecked) {
            toast.current.ShowToastFunction("warning", languageHome[activeLanguage].TOAST_WARNING_UNisChecked_PRIVACY)
        }
        else {
            setIsLoading(true)

            const formdata = new FormData();
            formdata.append('user_first_name', user_first_name)
            formdata.append('user_last_name', user_last_name)
            formdata.append('user_phone', user_phone[0] === '0' ? `${user_phone.substr(1, user_phone.length - 1)}` : user_phone)
            formdata.append('user_email', user_email)
            formdata.append('password', user_password)
            formdata.append('password_confirmation', user_password_confirmation)
            formdata.append('user_fcm_token', '-')
            formdata.append('user_country_phone', activePrefix)
            console.log(formdata)

            modelUser.register(formdata, res => {
                const { status, result } = res
                console.log(res)
                switch (status) {
                    case 200:
                        toast.current.ShowToastFunction("success", `${languageHome[activeLanguage].TOAST_SUCCESS_REGISTER_1}\n${user_first_name} ${user_last_name}\n${languageHome[activeLanguage].TOAST_SUCCESS_REGISTER_2}`)
                        _hideModal()
                        break;
                    case 401:
                        toast.current.ShowToastFunction("warning", languageHome[activeLanguage].TOAST_WARNING_DUPLICATE_NUMBER)
                        break;
                    default:
                        toast.current.ShowToastFunction("error", `-${status}-\nError`)
                        break;
                }
                setIsLoading(false)
            })
                .catch((err) => {
                    console.log(err)
                    toast.current.ShowToastFunction("error", GlobalLanguage[activeLanguage].TOAST_ERROR_DEFAULT)
                    setIsLoading(false)
                })
        }
    }

    const handleForgotPassword = () => {
        setIsLoading(true)

        if (!user_email) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_EMPTY_EMAIL)
            setIsLoading(false)
        }
        else if (!isEmail(user_email)) {
            toast.current.ShowToastFunction("warning", GlobalLanguage[activeLanguage].TOAST_WARNING_NOT_VALID_EMAIL)
            setIsLoading(false)
        }
        else {
            const formdata = new FormData();
            formdata.append('user_email', user_email)

            console.log(formdata)

            modelAuth.forgotPassword(formdata, res => {
                const { status, result } = res
                switch (status) {
                    case 200:
                        toast.current.ShowToastFunction("success", `${languageHome[activeLanguage].TOAST_SUCCESS_FORGOT_PASSWORD_1}\n${languageHome[activeLanguage].TOAST_SUCCESS_FORGOT_PASSWORD_2}`)
                        _hideModal()
                        break;
                    default:
                        toast.current.ShowToastFunction("warning", languageHome[activeLanguage].TOAST_WARNING_EMAIL_NOT_REGISTERED)
                        break;
                }
                setIsLoading(false)
            })
                .catch((err) => {
                    toast.current.ShowToastFunction("error", GlobalLanguage[activeLanguage].TOAST_ERROR_DEFAULT)
                    setIsLoading(false)
                })
        }
    }

    const changePrefix = () => {
        setIsModalVisible(false)
        Actions.multiOption({
            chosenType: 'phonePrefix',
            activePrefix: activePrefix
        })
    }

    const changeLanguage = () => {
        setIsLoading(true)
        Actions.multiOption({
            chosenType: 'language'
        })
        setIsLoading(false)
    }


    let modalContent = (
        <View style={styles.modalCenter}>
            <View style={{ width: '100%', height: 360 }}>
                <Swiper
                    onIndexChanged={onSwiperIndexChange}
                    horizontal
                    loop={false}
                    scrollEnabled={false}
                    ref={(ref) => { swiper = ref; }}
                    showsPagination={false}>
                    <View style={{ flex: 1 }}>
                        <View style={styles.modalHeader}>
                            <View style={{ position: 'absolute', top: 10, right: 10 }}>
                                <TouchableOpacity
                                    onPress={_hideModal}
                                    style={{ padding: 10, marginRight: 7 }}>
                                    <Ionicons name="md-close" size={30} color="black" />
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.dialogtitle}>{languageHome[activeLanguage].TEXT_MODAL_LOGIN_HEADER}</Text>
                        </View>
                        <View style={{ flex: 1 }}>
                            <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity onPress={changePrefix} style={{ height: 50, width: 80, justifyContent: 'space-around', alignItems: 'center', flexDirection: 'row', marginRight: -Metrics.SAFE_AREA, marginLeft: Metrics.SAFE_AREA, marginTop: 10 }}>
                                    <Text style={{ fontSize: 18, fontFamily: Fonts.APEX_MEDIUM, color: Colors.BLACK }}>{activePrefix}</Text>
                                    <AntDesign name="down" size={16} color={Colors.BLACK} />
                                </TouchableOpacity>
                                <View style={{ flex: 1 }}>
                                    <FloatingLabelInput
                                        label={GlobalLanguage[activeLanguage].INPUT_PHONE_NUMBER}
                                        placeholder="85XXXXXXXXX"
                                        autoCompleteType="tel"
                                        keyboardType="number-pad"
                                        value={user_phone}
                                        onChangeText={setUserPhone}
                                    />
                                </View>
                            </View>
                            <FloatingLabelInput
                                label={GlobalLanguage[activeLanguage].INPUT_PASSWORD}
                                autoCapitalize="none"
                                value={user_password}
                                autoCompleteType="password"
                                isPassword
                                onChangeText={setUserPassword}
                                onSubmitEditing={handleLogin}
                            />
                            <View style={styles.rememView}>
                                <TouchableOpacity style={{ padding: 10 }} onPress={() => changeSwiperIndex(1)}>
                                    <Text style={styles.forgot}>{languageHome[activeLanguage].TOUCHABLE_FORGOT_PASSWORD}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={[styles.modalFooter, { marginTop: Metrics.SAFE_AREA }]}>
                            <TouchableOpacity
                                style={styles.buttondialogsignup}
                                onPress={handleLogin}>
                                {isLoading ?
                                    <ActivityIndicator size={20} color={Colors.WHITE} /> :
                                    <Text style={styles.modelSignUp}>{languageHome[activeLanguage].BUTTON_LOGIN}</Text>}
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={[{ flex: 1 }, Platform.OS == "ios" && { marginRight: -Metrics.SAFE_AREA * 2, marginLeft: 2 * Metrics.SAFE_AREA }]}>
                        <View style={styles.modalHeader}>
                            <View style={{ position: 'absolute', top: 10, left: 10 }}>
                                <TouchableOpacity
                                    onPress={() => changeSwiperIndex(-1)}
                                    style={{ padding: 10, marginRight: 7, alignItems: 'center', flexDirection: 'row' }}
                                >
                                    <Ionicons name="md-arrow-back" size={30} color="black" />
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.dialogtitleLong}>{languageHome[activeLanguage].TEXT_MODAL_FORGOT_PASSWORD_HEADER}</Text>
                            <View style={{ position: 'absolute', top: 10, right: 10 }}>
                                <TouchableOpacity
                                    onPress={_hideModal}
                                    style={{ padding: 10, marginRight: 7 }}
                                >
                                    <Ionicons name="md-close" size={30} color="black" />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <View style={{ alignSelf: 'center', marginTop: Metrics.SAFE_AREA, width: '80%' }}>
                                <Text style={[styles.terms, { textAlign: 'center' }]}>{languageHome[activeLanguage].TEXT_MODAL_FORGOT_PASSWORD_BODY_1}</Text>
                            </View>
                            <FloatingLabelInput
                                label={GlobalLanguage[activeLanguage].INPUT_EMAIL}
                                autoCapitalize="none"
                                autoCompleteType="email"
                                keyboardType="email-address"
                                value={user_email}
                                onChangeText={setUserEmail}
                            />
                            <View style={{ alignSelf: 'center', marginVertical: Metrics.SAFE_AREA, width: '80%' }}>
                                <Text style={[styles.terms, { textAlign: 'center' }]}>{languageHome[activeLanguage].TEXT_MODAL_FORGOT_PASSWORD_BODY_2}</Text>
                            </View>
                        </View>
                        <View style={[styles.modalFooter, { marginTop: Metrics.SAFE_AREA }]}>
                            <TouchableOpacity
                                style={styles.buttondialogsignup}
                                onPress={handleForgotPassword}>
                                {isLoading ?
                                    <ActivityIndicator size={20} color={Colors.WHITE} /> :
                                    <Text style={styles.modelSignUp}>{languageHome[activeLanguage].BUTTON_SEND_EMAIL}</Text>}
                            </TouchableOpacity>
                        </View>
                    </View>
                </Swiper>
            </View>
        </View>
    )

    if (isModalRegister) {
        modalContent = (
            <View style={styles.modalCenter}>
                <View style={styles.modalHeader}>
                    <View style={{ position: 'absolute', top: 10, right: 10 }}>
                        <TouchableOpacity
                            onPress={_hideModal}
                            style={{ padding: 10, marginRight: 7 }}
                        >
                            <Ionicons name="md-close" size={30} color="black" />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.dialogtitle}>{languageHome[activeLanguage].TEXT_MODAL_REGISTER_HEADER}</Text>
                </View>
                <View>
                    <ScrollView
                        onScroll={({ nativeEvent }) => {
                            if (isCloseToBottom(nativeEvent)) setIsVisibleArrow(false)
                            else setIsVisibleArrow(true)
                        }}
                        scrollEventThrottle={16}
                        style={{ maxHeight: Metrics.SCREEN_HEIGHT * 0.5, paddingBottom: Metrics.SAFE_AREA }}
                        showsVerticalScrollIndicator>
                        <FloatingLabelInput
                            label={GlobalLanguage[activeLanguage].INPUT_FIRST_NAME}
                            value={user_first_name}
                            onChangeText={setUserFirstName}
                            autoCompleteType="name"
                            maxLength={50}
                        />
                        <FloatingLabelInput
                            label={GlobalLanguage[activeLanguage].INPUT_LAST_NAME}
                            value={user_last_name}
                            onChangeText={setUserLastName}
                            helperText={GlobalLanguage[activeLanguage].HELPER_OPTIONAL}
                            maxLength={50}
                        />
                        <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                            <TouchableOpacity onPress={changePrefix} style={{ height: 50, width: 80, justifyContent: 'space-around', alignItems: 'center', flexDirection: 'row', marginRight: -Metrics.SAFE_AREA, marginLeft: Metrics.SAFE_AREA }}>
                                <Text style={{ fontSize: 18, fontFamily: Fonts.APEX_MEDIUM, color: Colors.BLACK }}>{activePrefix}</Text>
                                <Entypo name="chevron-thin-down" size={20} color={Colors.BLACK} />
                            </TouchableOpacity>
                            <View style={{ flex: 1 }}>
                                <FloatingLabelInput
                                    label={GlobalLanguage[activeLanguage].INPUT_PHONE_NUMBER}
                                    placeholder="85XXXXXXXXX"
                                    autoCompleteType="tel"
                                    keyboardType="number-pad"
                                    value={user_phone}
                                    onChangeText={setUserPhone}
                                    maxLength={15}
                                />
                            </View>
                        </View>
                        <FloatingLabelInput
                            label={GlobalLanguage[activeLanguage].INPUT_EMAIL}
                            autoCapitalize="none"
                            keyboardType="email-address"
                            autoCompleteType="email"
                            value={user_email}
                            onChangeText={setUserEmail}
                            maxLength={50}
                        />
                        <View style={{ width: "100%", justifyContent: 'center', marginHorizontal: 20 }}>
                            <Text style={{ fontSize: 14, fontFamily: Fonts.APEX_REGULAR }}>{languageHome[activeLanguage].TEXT_PASSWORD_HEADER}</Text>
                            <View style={{ flexDirection: 'row', width: "100%", marginVertical: 2 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
                                    <Ionicons
                                        name={isOneLowerCase ? 'checkmark-circle-outline' : 'close-circle-outline'}
                                        style={[isOneLowerCase ? { color: Colors.GREEN, marginRight: 4 } : { color: Colors.RED, marginRight: 4 }]}
                                    />
                                    <Text style={{ fontSize: 14, fontFamily: Fonts.APEX_REGULAR, textAlign: 'center' }}>{languageHome[activeLanguage].TEXT_VALIDATION_LOWERCASE}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
                                    <Ionicons
                                        name={isOneDigit ? 'checkmark-circle-outline' : 'close-circle-outline'}
                                        style={[isOneDigit ? { color: Colors.GREEN, marginRight: 4 } : { color: Colors.RED, marginRight: 4 }]}
                                    />
                                    <Text style={{ fontSize: 14, fontFamily: Fonts.APEX_REGULAR, textAlign: 'center' }}>{languageHome[activeLanguage].TEXT_VALIDATION_ONEDIGIT}</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', width: "100%", marginVertical: 2 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
                                    <Ionicons
                                        name={isOneUpperCase ? 'checkmark-circle-outline' : 'close-circle-outline'}
                                        style={[isOneUpperCase ? { color: Colors.GREEN, marginRight: 4 } : { color: Colors.RED, marginRight: 4 }]}
                                    />
                                    <Text style={{ fontSize: 14, fontFamily: Fonts.APEX_REGULAR, textAlign: 'center' }}>{languageHome[activeLanguage].TEXT_VALIDATION_UPPERCASE}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
                                    <Ionicons
                                        name={isAbove8Char ? 'checkmark-circle-outline' : 'close-circle-outline'}
                                        style={[isAbove8Char ? { color: Colors.GREEN, marginRight: 4 } : { color: Colors.RED, marginRight: 4 }]}
                                    />
                                    <Text style={{ fontSize: 14, fontFamily: Fonts.APEX_REGULAR, textAlign: 'center' }}>{languageHome[activeLanguage].TEXT_VALIDATION_ABOVE8CHAR}</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', width: "100%", marginVertical: 2 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
                                    <Ionicons
                                        name={isOneSymbol ? 'checkmark-circle-outline' : 'close-circle-outline'}
                                        style={[isOneSymbol ? { color: Colors.GREEN, marginRight: 4 } : { color: Colors.RED, marginRight: 4 }]}
                                    />
                                    <Text style={{ fontSize: 14, fontFamily: Fonts.APEX_REGULAR, textAlign: 'center' }}>{languageHome[activeLanguage].TEXT_VALIDATION_SYMBOL}</Text>
                                </View>
                            </View>
                        </View>
                        <FloatingLabelInput
                            label={GlobalLanguage[activeLanguage].INPUT_PASSWORD}
                            autoCapitalize="none"
                            value={user_password}
                            autoCompleteType="password"
                            isPassword
                            // helperText={GlobalLanguage[activeLanguage].HELPER_PASSWORD}
                            // onChangeText={setUserPassword}
                            onChangeText={onTextChangePassword}
                            maxLength={50}
                        />
                        <FloatingLabelInput
                            label={GlobalLanguage[activeLanguage].INPUT_CONFIRM_PASSWORD}
                            autoCapitalize="none"
                            value={user_password_confirmation}
                            isPassword
                            onChangeText={setUserPasswordConfirmation}
                        />
                        <View style={{ alignSelf: 'center', marginVertical: 10, height: 40, flexDirection: 'row', width: '80%' }}>
                            <TouchableOpacity onPress={() => setIsChecked(!isChecked)} style={{ height: '100%', justifyContent: 'center', marginRight: 5 }}>
                                <MaterialIcons
                                    size={28}
                                    name={isChecked ? 'check-box' : 'check-box-outline-blank'}
                                    color={isChecked ? Colors.RED : Colors.GRAY}
                                />
                            </TouchableOpacity>
                            <Text style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                                <Text style={styles.terms}>{languageHome[activeLanguage].TEXT_MODAL_REGISTER_BODY_1} </Text>
                                <Text style={styles.termstwo} onPress={() => { Linking.openURL("http://www.cekori.id/terms-of-service") }}>{languageHome[activeLanguage].TOUCHABLE_TERMS_CONDITION} </Text>
                                <Text style={styles.termsthree}> {languageHome[activeLanguage].TEXT_MODAL_REGISTER_BODY_2} </Text>
                                <Text style={styles.termstwo} onPress={() => { Linking.openURL("http://www.cekori.id/privacy") }}> {languageHome[activeLanguage].TOUCHABLE_PRIVACY_POLICY} </Text>
                                <Text style={styles.termsthree}>{languageHome[activeLanguage].TEXT_MODAL_REGISTER_BODY_3}</Text>
                            </Text>
                        </View>
                    </ScrollView>
                    {isVisibleArrow &&
                        <View style={{ position: 'absolute', bottom: 10, width: '100%', alignItems: 'center' }}>
                            <Entypo name="chevron-thin-down" size={20} color={Colors.BLACK} />
                        </View>}
                </View>
                <View style={styles.modalFooter}>
                    <TouchableOpacity
                        style={styles.buttondialogsignup}
                        onPress={handleRegister}
                    >
                        {isLoading ? <ActivityIndicator size={20} color={Colors.WHITE} />
                            : <Text style={styles.modelSignUp}>{languageHome[activeLanguage].BUTTON_CREATE_ACCOUNT}</Text>}
                    </TouchableOpacity>
                </View>
            </View >
        )
    }

    return (
        <ImageBackground source={Images.HOME_BACKGROUND} style={{ flex: 1 }}>
            <View style={{ position: 'absolute', top: 40, right: 24 }}>
                <TouchableOpacity
                    disabled={isLoading}
                    onPress={changeLanguage}
                    style={[
                        Platform.OS == 'ios' && { paddingTop: 4, paddingLeft: 1 },
                        { height: 36, width: 36, justifyContent: 'center', alignItems: 'center', borderRadius: 18, borderWidth: 2, borderColor: Colors.RED, backgroundColor: Colors.WHITE }
                    ]}>
                    <Text style={{ fontFamily: Fonts.APEX_BOLD, fontSize: 14, letterSpacing: 0.5, color: Colors.RED, textAlign: 'center' }}>{activeLanguage}</Text>
                </TouchableOpacity>
            </View>
            {!isModalVisible && (
                <>
                    <View style={{ marginTop: Metrics.SCREEN_HEIGHT * 0.4, alignSelf: 'center', paddingHorizontal: Metrics.SAFE_AREA }}>
                        <Text style={styles.headertext}>{languageHome[activeLanguage].TEXT_BACKGROUND_TITLE}</Text>
                        <View style={[
                            Platform.OS == 'ios' && { marginTop: 5 },
                            { flexDirection: 'row', alignSelf: 'center', height: 38 }
                        ]}>
                            <Text style={styles.headertext}>CEK ORI</Text>
                            <Text style={{ fontSize: 18 }}>®</Text>
                        </View>
                        <Text style={styles.desctext}>{languageHome[activeLanguage].TEXT_BACKGROUND_BODY}</Text>
                    </View>
                    <ScrollView style={styles.form}>
                        {isLoading ? (
                            <View style={{ height: 80, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                <ActivityIndicator color={Colors.RED} size={18} />
                            </View>
                        ) : (
                                <View style={{ flex: 1, flexDirection: 'row', width: '100%' }}>
                                    <View style={{ flex: 1 }}>
                                        <TouchableOpacity
                                            activeOpacity={1} ter
                                            style={styles.buttonlogin}
                                            onPress={openLoginModal}
                                        >
                                            <Text style={styles.btnText}>{languageHome[activeLanguage].BUTTON_LOGIN}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <TouchableOpacity
                                            activeOpacity={1}
                                            style={styles.buttonRegister}
                                            onPress={openRegisterModal}
                                        >
                                            <Text style={styles.btnText}>{languageHome[activeLanguage].BUTTON_REGISTER}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )}
                    </ScrollView>
                </>
            )}

            <Modal
                animationType="slide"
                transparent
                children={modalContent}
                style={{ margin: Metrics.SAFE_AREA }}
                visible={isModalVisible}
                avoidKeyboard={Platform.OS == 'ios'}
            />
            <CustomToast ref={toast} />
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    headertext: {
        fontFamily: Fonts.APEX_MEDIUM,
        color: Colors.BLACK,
        textAlign: 'center',
        fontSize: 34
    },

    desctext: {
        fontFamily: Fonts.APEX_REGULAR,
        textAlign: 'center',
        alignSelf: 'center',
        lineHeight: 24,
        fontSize: 18,
        color: Colors.BLACK,
        marginTop: Metrics.SCREEN_HEIGHT * 0.04
    },

    form: {
        marginTop: 50,
        width: '100%',
        paddingHorizontal: Metrics.SAFE_AREA,
        alignSelf: 'center',
        margin: 20,
        marginTop: Metrics.SCREEN_HEIGHT * 0.05
    },

    buttonlogin: {
        backgroundColor: Colors.RED,
        alignSelf: 'center',
        height: Metrics.SCREEN_HEIGHT * 0.08,
        width: Metrics.SCREEN_WIDTH * 0.4,
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },

    buttonRegister: {
        backgroundColor: Colors.BLACK,
        alignSelf: 'center',
        height: Metrics.SCREEN_HEIGHT * 0.08,
        width: Metrics.SCREEN_WIDTH * 0.4,
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },

    buttondialogsignup: {
        backgroundColor: Colors.RED,
        alignSelf: 'center',
        borderRadius: 40,
        height: Metrics.SCREEN_HEIGHT * 0.07,
        width: 200,
        alignItems: 'center',
        justifyContent: 'center'
    },

    btnText: {
        fontFamily: Fonts.APEX_MEDIUM,
        color: Colors.WHITE,
        fontSize: 16
    },

    modalCenter: {
        borderRadius: 20,
        width: '100%',
        backgroundColor: Colors.WHITE,
        // paddingHorizontal: 0,
        marginTop: Metrics.SCREEN_HEIGHT > 640 ? 150 : 100,
        overflow: 'hidden'
    },

    inputJenis: {
        fontFamily: Fonts.APEX_REGULAR,
        color: Colors.GRAY_LIGHT
    },

    rememView: {
        marginTop: 20,
        width: Metrics.SCREEN_WIDTH * 0.80,
        alignSelf: 'center',
        alignItems: 'center'
    },

    remem: {
        marginLeft: 5,
        fontSize: 15,
    },

    forgot: {
        fontSize: 16,
        fontFamily: Fonts.APEX_REGULAR,
        color: Colors.BLACK
    },

    modelSignUp: {
        fontFamily: Fonts.APEX_MEDIUM,
        color: Colors.WHITE
    },

    dialogtitle: {
        color: Colors.BLACK,
        fontSize: 25,
        fontFamily: Fonts.APEX_MEDIUM
    },

    dialogtitleLong: {
        color: Colors.BLACK,
        fontSize: 20,
        fontFamily: Fonts.APEX_MEDIUM
    },

    terms: {
        fontFamily: Fonts.APEX_REGULAR,
        color: Colors.GRAY,
        fontSize: 12,
        lineHeight: 18
    },

    termstwo: {
        fontFamily: Fonts.APEX_REGULAR,
        color: Colors.BLUE,
        fontSize: 12,
    },

    termsthree: {
        fontFamily: Fonts.APEX_REGULAR,
        fontSize: 12,
        color: Colors.GRAY
    },

    modalHeader: {
        height: 70,
        width: '100%',
        shadowColor: Colors.BLACK,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.WHITE
    },

    modalFooter: {
        height: 80,
        width: '100%',
        shadowColor: Colors.BLACK,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.WHITE
    }

});

const mapStateToProps = state => {
    return {
        activeLanguage: state.languageOperation.activeLanguage
    };
};

export default connect(mapStateToProps)(HomeScreen);