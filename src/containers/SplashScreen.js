import React, { useEffect, useRef, useState } from 'react';
import {
	View,
	Image,
	Platform
} from 'react-native';
import { Colors, StorageKey, Images, Version } from '../GlobalConfig';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';

import { wait, getUserData } from '../GlobalFunction';
import { changeLanguage } from '../redux/actions/actionTypes';
import { modelUser } from '../models/User';
import GlobalLanguage from '../GlobalLanguage';
import CustomToast from '../components/CustomToast';

const SplashScreen = (props) => {
	const { activeLanguage } = props
	const [appType, setAppType] = useState("1")
	const [appVersion, setAppVersion] = useState(null)
	const [osType, setOsType] = useState(null)
	const toast = useRef(null)

	useEffect(() => {
		wait
		if (Platform.OS == "android") {
			setOsType(1)
		}
		else {
			setOsType(2)
		}
		setAppVersion(Version)
		wait(2000).then(() => checkUpdate())
		AsyncStorage.getItem(StorageKey.Language)
			.then(
				)
				val => {
					if (val) props.changeLanguage(val)
				},
				err => console.warn(err)
	}, [])

	checkUpdate = () => {
		const params_data = `MAVER_APP_VERSION=${appVersion}&MAVER_APP_TYPE=${appType}&MAVER_OS_TYPE=${osType}`
		modelUser.checkUpdate(params_data, res => {
			const { status, result } = res
			console.log(res)
			switch (status) {
				case 200:
					Actions.updateApp({ message: result.MAVER_NOTES, priority: result.MAVER_IS_PRIORITY })
					break;
				case 404:
					wait(2000)
					.then(() => {
							getUserData()
								.then((res) => {
									if (res) Actions.master()
									else Actions.home()
								})
								.catch(err => Actions.home())
						})
					break;
					case 400:
					switch (result.err_code) {
						case "E1":
							toastRef.current.ShowToastFunction("error", GlobalLanguage[activeLanguage].TOAST_ERROR_ER01)
							break;
					}
					break;
				case 500:
					wait(2000)
						.then(() => {
							getUserData()
								.then((res) => {
									if (res) Actions.master()
									else Actions.home()
								})
								.catch(err => Actions.home())
						})
					break;
				default:
					toast.current.ShowToastFunction("error", GlobalLanguage[activeLanguage].TOAST_ERROR_DEFAULT)
					break;
			}
		})
			.catch((error) => {
				console.log(error.message);
				toast.current.ShowToastFunction("error", GlobalLanguage[activeLanguage].TOAST_ERROR_DEFAULT)
			})
	}

	return (
		<View style={{ flex: 1, backgroundColor: Colors.RED, alignItems: "center", justifyContent: "center" }}>
			<Image resizeMethod="resize" fadeDuration={0} source={Images.LOGO} style={{ flex: 0.7, width: "80%", height: "auto", resizeMode: "contain" }} />
			<CustomToast ref={toast} />
		</View>
	);
}

const mapStateToProps = state => {
	return {
		activeLanguage: state.languageOperation.activeLanguage
	};
};

const mapDispatchToProps = dispatch => {
	return {
		changeLanguage: language => {
			dispatch(changeLanguage(language));
		},
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);