import React, { useEffect, useRef, useState } from 'react';
import { Text, View, TouchableOpacity, Animated, ActivityIndicator, Image, StyleSheet } from 'react-native';
import { FloatingAction } from 'react-native-floating-action';
import Icon from "react-native-vector-icons/FontAwesome";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Entypo from "react-native-vector-icons/Entypo";
import { Actions } from 'react-native-router-flux'
import AsyncStorage from '@react-native-community/async-storage'
import Share from 'react-native-share';
import { connect } from 'react-redux';

import { wait, getUserData } from '../../GlobalFunction';
import { Colors, StorageKey, Images, StoreURL, Fonts } from '../../GlobalConfig';
import { modelAuth } from '../../models/Auth';

import CustomToast from '../../components/CustomToast';
import languageMaster from './languageMaster';
import GlobalLanguage from '../../GlobalLanguage';
import { modelUser } from '../../models/User';

const actionDefaultProps = {
	textBackground: 'transparent',
	textColor: '#fff',
	textElevation: 0,
	color: Colors.RED,
}

const iconStyle = {
	fontSize: 16,
	color: Colors.WHITE
}

const MasterScreen = (props) => {
	const { activeLanguage } = props
	const [isTextHidden, setIsTextHidden] = useState(false)
	const [isLoading, setIsLoading] = useState(false)
	const [isRefresh, setIsRefresh] = useState(false)
	const [isAllowLocation, setIsAllowLocation] = useState(false)
	const [data, setData] = useState("")
	const toast = useRef(null)
	animatedUpEffect = new Animated.Value(0)


	useEffect(() => {
        getUserData().then(result => {
            const { user_id } = result
            modelUser.getProfile(user_id, res => {
                const { status, result } = res
                console.log(res)
                switch (status) {
                    case 200:
                        AsyncStorage.setItem(StorageKey.userFirstName, JSON.stringify(result.message.user_first_name))
                        AsyncStorage.setItem(StorageKey.userLastName, JSON.stringify(result.message.user_last_name))
                        break;
                    default:
                        break;
                }
            })
                .catch((error) => {
                    toast.current.ShowToastFunction("error", GlobalLanguage[activeLanguage].TOAST_ERROR_DEFAULT)
                    setIsLoading(false)
                    console.log(error)
                })
        })
    }, [])

	useEffect(() => {
		if (props.activeLanguage) {
			wait(0).then(() => Actions.refresh({ title: languageMaster[props.activeLanguage].TEXT_HEADER }))
		}
	}, [props.activeLanguage])


	const handleFABPress = (name) => {
		showText()
		switch (name) {
			case 'profile':
				Actions.profil()
				break;
			case 'info':
				Actions.info()
				break;
			case 'language':
				Actions.multiOption({
					chosenType: 'language'
				})
				break;
			case 'share':
				const options = {
					message: GlobalLanguage[activeLanguage].TEXT_CHECK_PRODUCT,
					url: StoreURL
				};
				Share.open(options)
					.then((res) => { console.log(res) })
					.catch((err) => { console.log(err) })
				break;
			case 'logout':
				logout()
				break;
			default:
				break;
		}
	}

	const goToQRScanner = () => {
		showText()
		Actions.qrcode()
	}

	const logout = () => {
		getUserData().then(result => {
            const {user_id} = result
		setIsLoading(true)
		let formdata = new FormData();
		formdata.append('user_id', user_id)

		modelAuth.logout(formdata, res => {
			const { status, result } = res
			switch (status) {
				case 200:
					toast.current.ShowToastFunction("success", languageMaster[activeLanguage].TOAST_SUCCESS_LOGOUT)
					let keys = [StorageKey.UserData]
					AsyncStorage.multiRemove(keys)
					wait(1000).then(() => {
						setIsLoading(false)
						Actions.home()
					})
					break;
				default:
					toast.current.ShowToastFunction("warning", languageMaster[activeLanguage].TOAST_WARNING_FAILED_LOGOUT)
					break;
			}
		})
			.catch((err) => {
				toast.current.ShowToastFunction("error", GlobalLanguage[activeLanguage].TOAST_ERROR_DEFAULT)
			});
		})
	}

	const hideText = () => {
		Animated.timing(animatedUpEffect, {
			toValue: 1,
			duration: 100,
			useNativeDriver: true
		}).start();
		setIsTextHidden(true)
	}

	const showText = () => {
		Animated.timing(animatedUpEffect, {
			toValue: 0,
			duration: 100,
			useNativeDriver: true
		}).start();
		setIsTextHidden(false)
	}


	const actions = [
		{
			...actionDefaultProps,
			text: languageMaster[activeLanguage].TEXT_OPTION_PROFILE,
			icon: <Icon name="user" style={iconStyle} />,
			name: 'profile',
			position: 1,
		},
		{
			...actionDefaultProps,
			text: languageMaster[activeLanguage].TEXT_OPTION_INFO,
			icon: <SimpleLineIcons name="info" style={iconStyle} />,
			name: 'info',
			position: 2
		},
		{
			...actionDefaultProps,
			text: languageMaster[activeLanguage].TEXT_OPTION_CHANGE_LANGUAGE,
			icon: <Entypo name="language" style={iconStyle} />,
			name: 'language',
			position: 3
		},
		{
			...actionDefaultProps,
			text: languageMaster[activeLanguage].TEXT_OPTION_SHARE,
			icon: <SimpleLineIcons name="action-redo" style={iconStyle} />,
			name: 'share',
			position: 4
		},
		{
			...actionDefaultProps,
			text: languageMaster[activeLanguage].TEXT_OPTION_LOGOUT,
			icon: <Icon name="sign-out" style={iconStyle} />,
			name: 'logout',
			position: 5
		}
	]

	const heightIncrease = animatedUpEffect.interpolate({
		inputRange: [0, 1],
		outputRange: [0, -100]
	});

	return (
		<View style={{ flex: 1 }}>
			<View style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', flex: 1 }}>
				<Text style={[styles.headertext, (isTextHidden || isLoading) && { opacity: 0 }]}>{languageMaster[activeLanguage].TEXT_BODY}</Text>
				<Animated.View style={{ marginVertical: 15, transform: [{ translateY: heightIncrease }] }}>
					{isLoading ? <ActivityIndicator color={Colors.RED} size="large" />
						: (
							<TouchableOpacity onPress={goToQRScanner}>
								<Image
									source={Images.QR_BUTTON}
									style={{ height: 100, width: 100 }}
									resizeMode='cover' />
							</TouchableOpacity>
						)}
				</Animated.View>
				<TouchableOpacity disabled={isTextHidden || isLoading} style={(isTextHidden || isLoading) && { opacity: 0 }} onPress={goToQRScanner}>
					<Text style={styles.buttonBigTextScan}>{languageMaster[activeLanguage].TEXT_SHOW_NOW}</Text>
				</TouchableOpacity>
			</View>

			<FloatingAction
				ref={(ref) => { floatingAction = ref; }}
				actions={actions}
				color={isLoading ? Colors.GRAY : Colors.RED}
				overlayColor={'rgba(0,0,0,0.7)'}
				distanceToEdge={20}
				onPressBackdrop={showText}
				onPressMain={isLoading ? null
					: isTextHidden ?
						showText
						: hideText}
				onPressItem={handleFABPress} />
			<CustomToast ref={toast} />
		</View>
	);
}

const styles = StyleSheet.create({
	headertext: {
		fontFamily: Fonts.APEX_MEDIUM,
		color: Colors.BLACK,
		textAlign: 'center',
		fontSize: 24
	},
	buttonBigTextScan: {
		fontFamily: Fonts.APEX_BOLD,
		color: Colors.BLACK,
		textAlign: 'center',
		fontSize: 28,
		letterSpacing: 1
	},

});

const mapStateToProps = state => {
	return {
		activeLanguage: state.languageOperation.activeLanguage
	};
};

export default connect(mapStateToProps)(MasterScreen);