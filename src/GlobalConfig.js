import { Dimensions, Platform } from "react-native"

export const Fonts = {
    APEX_BOLD: 'ApexNew-Bold',
    APEX_REGULAR: 'ApexNew-Book',
    APEX_MEDIUM: 'ApexNew-Medium',
    MINION_REGULAR: 'MinionPro-Regular'
}

export const Colors = {
    RED_DARK: "#5e0003",
    RED: '#910005',
    GREEN: '#3A8048',
    BLACK: '#000000',
    GRAY: '#a7a9ab',
    GRAY_LIGHT: '#efefef',
    WHITE: '#ffffff',
    BLUE : '#0691ce'
}

export const PropertyColors = {
    SUCCESS_COLOR: Colors.GREEN,
    WARNING_COLOR: Colors.BLACK,
    ERROR_COLOR: Colors.RED
}

export const Metrics = Object.freeze({
    SAFE_AREA: 16,
    SCREEN_WIDTH: Dimensions.get('window').width,
    SCREEN_HEIGHT: Dimensions.get('window').height,
    NAVBAR_HEIGHT: 56
})

export const WarningMessage = {
    ErrorMessage: 'Koneksi tidak tersedia'
}

export const StorageKey = {
    UserData: 'user_data',
    Language: 'language',
    userFirstName: 'user_first_name',
    userLastName: 'user_last_name'
}

export const Images = {
    HOME_BACKGROUND: require('./images/backGroundScreen.jpg'),
    QR_BUTTON: require('./images/qr-icon.png'),
    LOGO: require("./images/logo.png")
}

export const ArrayOfPhonePrefix = [
    {
        countryName: 'Indonesia',
        prefix: '+62'
    },
    {
        countryName: 'China',
        prefix: '+86'
    },
]

export const ArrayOfLanguage = [
    {
        languageName: 'Bahasa Indonesia',
        code: 'ID'
    },
    {
        languageName: 'English',
        code: 'EN'
    },
]


export const Version = Platform.OS == 'android' ? '1.7.1' : '1.2.0'

export const StoreURL = Platform.OS == 'android' ? 'https://play.google.com/store/apps/details?id=com.cekoriv2' : 'https://apps.apple.com/us/app/cekori/id1478556984'