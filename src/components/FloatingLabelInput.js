import React, { PureComponent } from 'react';
import { View, TextInput, Animated, Text, TouchableOpacity, Platform } from 'react-native';
import FontAwesome from "react-native-vector-icons/FontAwesome";

import { Colors, Metrics, Fonts } from '../GlobalConfig';

export default class FloatingLabelInput extends PureComponent {
	constructor(props) {
		super(props);
		this._animatedIsFocused = this.props.bottomOnly || this.props.value ? new Animated.Value(1) : new Animated.Value(0);
		this.state = {
			isFocused: false,
			showPassword: false
		};
	}

	componentDidUpdate() {
		Animated.timing(this._animatedIsFocused, {
			toValue: this.state.isFocused ? 1 : this.props.value ? 1 : 0,
			duration: 100,
			useNativeDriver: false
		}).start();
	}

	handleFocus = () => this.setState({ isFocused: true });
	handleBlur = () => this.setState({ isFocused: false });

	togglePassword = () => this.setState({ showPassword: !this.state.showPassword })

	render() {
		const { label, ...props } = this.props;
		const labelStyle = {
			position: 'absolute',
			zIndex: 1,
			backgroundColor: Colors.WHITE,
			fontFamily: Fonts.APEX_MEDIUM,
			paddingHorizontal: this._animatedIsFocused.interpolate({
				inputRange: [0, 0.75, 1],
				outputRange: [0, 0, 4],
			}),
			left: this._animatedIsFocused.interpolate({
				inputRange: [0, 1],
				outputRange: [16, 10],
			}),
			top: this._animatedIsFocused.interpolate({
				inputRange: [0, 1],
				outputRange: Platform.OS == 'android' ? this.props.multiline ? [86, 10] : [28, 10] : this.props.multiline ? [91, 14] : [33, 14],
			}),
			fontSize: this._animatedIsFocused.interpolate({
				inputRange: [0, 1],
				outputRange: [18, 12],
			}),
			color: this._animatedIsFocused.interpolate({
				inputRange: [0, 1],
				outputRange: [Colors.GRAY, Colors.RED],
			}),
		};
		const inputBoxContainerStyle = {
			height: this.props.multiline ? 100 : 45,
			width: '100%',
			flexDirection: 'row',
			borderWidth: 1.5,
			borderRadius: 10,
		};
		const inputBoxContainerBottomStyle = {
			height: this.props.multiline ? 100 : 45,
			width: '100%',
			flexDirection: 'row',
			borderBottomWidth: 1.5,
		};
		const inputBoxStyle = {
			flex: 1,
			fontSize: 18,
			color: '#000',
			paddingHorizontal: 14,
		};
		const helperStyle = {
			fontSize: 10,
			fontFamily: Fonts.APEX_REGULAR
		};
		const maxLengthStyle = {
			fontSize: 10,
			fontFamily: Fonts.APEX_REGULAR,
			textAlign: 'right',
			width: 70
		};
		const helperContainerStyle = {
			width: '100%',
			justifyContent: 'space-between',
			flexDirection: 'row',
			paddingHorizontal: Metrics.SAFE_AREA
		};
		return (
			<View style={{ paddingTop: 18, marginHorizontal: Metrics.SAFE_AREA + 2, marginBottom: 10 }}>
				<Animated.Text style={labelStyle}>
					{label}
				</Animated.Text>
				<View style={[this.props.bottomOnly ? inputBoxContainerBottomStyle : inputBoxContainerStyle,
				this.state.isFocused ? this.props.bottomOnly ? { borderBottomColor: Colors.RED } : { borderColor: Colors.RED } : props.value ? this.props.bottomOnly ? { borderBottomColor: Colors.GRAY } : { borderColor: Colors.GRAY } : this.props.bottomOnly ? { borderBottomColor: Colors.GRAY } : { borderColor: Colors.GRAY, zIndex: 2 }]}>
					<TextInput
						{...props}
						secureTextEntry={props.isPassword && !this.state.showPassword}
						placeholder={this.state.isFocused && props.placeholder ? props.placeholder : ""}
						placeholderTextColor={Colors.GRAY}
						selectionColor={Colors.RED}
						style={[inputBoxStyle, this.props.multiline && { textAlignVertical: 'bottom' }]}
						onFocus={this.handleFocus}
						onBlur={this.handleBlur}
						blurOnSubmit={this.props.multiline ? false : true}
						numberOfLines={3}
					/>
					{props.isPassword ?
						<TouchableOpacity onPress={this.togglePassword} style={{ width: 45, height: '100%', justifyContent: 'center', alignItems: 'center', zIndex: 3 }}>
							<FontAwesome
								size={28}
								name={this.state.showPassword ? 'eye' : 'eye-slash'}
								color={this.state.showPassword ? Colors.BLACK : Colors.GRAY}
							/>
						</TouchableOpacity> : null}
				</View>
				<View style={[helperContainerStyle, Platform.OS == 'ios' && { marginTop: 2 }]}>
					{props.helperText ? <Text style={helperStyle}>{props.helperText}</Text> : <View />}
					{props.maxLength ? <Text style={maxLengthStyle}>{props.value ? props.value.length : '0'} / {props.maxLength}</Text> : <View />}
				</View>
			</View>
		);
	}
}