import AsyncStorage from '@react-native-community/async-storage'
import { StorageKey } from '../GlobalConfig'

export default async (update, API, requestType = 'GET', useToken = false, storageId, post_data = null, params_data = null, debugMode = false) => {
    let token
    if (useToken) {
        AsyncStorage.getItem(StorageKey.UserData, (err, res) => {
            token = JSON.parse(res).user_token
            console.log('token ' + token)
        })
    }

    // If the data cache is available, update with the cached data first
    AsyncStorage.getItem(storageId, (error, result) => {
        if (result) {
            result = JSON.parse(result)
            update(result ?
                {
                    "isCache": true,
                    "result": result
                } :
                {
                    "isCache": true,
                    "result": []
                }
            )
        }
    })

    // Request the latest data from the server, if successful, update the view and update the cache
    return fetch(`${API}${params_data ? `?${params_data}` : ``}`, {
        method: requestType,
        headers: post_data ?
            {
                'Accept': 'multipart/form-data',
                'Content-Type': 'multipart/form-data',
                'X-Api-Key': token
            } :
            {
                'Authorization': `Bearer ${token}`
            },
        body: post_data ? post_data : ''
    })
        .then((response) => {
            if (debugMode) {
                console.log(response.text())
                console.log(response.status)
            }
            const statusCode = response.status;
            const res = response.status == 200 ? response.json() : []
            return Promise.all([statusCode, res])
        })
        .then(([statusCode, result]) => {
            if (statusCode == 200) {
                update(
                    {
                        "status": statusCode,
                        "result": result,
                        "isCache": false
                    }
                )
                AsyncStorage.setItem(storageId, JSON.stringify(result))
            }
        })
        .catch(err => { throw err })

}