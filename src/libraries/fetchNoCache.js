import AsyncStorage from '@react-native-community/async-storage'
import { StorageKey } from '../GlobalConfig'

export default async (update, API, requestType = 'GET', useToken = false, post_data = null, params_data = null, debugMode = false) => {
    let token
    if (useToken) {
        AsyncStorage.getItem(StorageKey.UserData, (err, res) => {
            token = JSON.parse(res).user_token
            console.log('token ' + token)
        })
    }

    return fetch(`${API}${params_data ? `?${params_data}` : ``}`, {
        method: requestType,
        headers: post_data ?
            {
                'Accept': 'multipart/form-data',
                'Content-Type': 'multipart/form-data',
                'X-Api-Key': token
            } :
            {
                'X-Api-Key': token
            },
        body: post_data ? post_data : ''
    })
        .then((response) => {
            if (debugMode) {
                console.log({
                    API: `${API}${params_data ? `?${params_data}` : ``}`,
                    post_data: post_data
                })
                console.log(response.text())
                console.log(response.status)
            }
            const statusCode = response.status;
            const res = response.json()
            return Promise.all([statusCode, res])
        })
        .then(([statusCode, result]) => {
            update(
                {
                    "status": statusCode,
                    "result": result,
                    "isCache": false
                }
            )
        })
        .catch(err => { throw err })

}