import React, { Component } from 'react'
import { Scene, Router, Actions, Stack } from 'react-native-router-flux'
import {
    Easing,
    Alert,
    BackHandler,
    StyleSheet,
    Platform
} from 'react-native';

// screens identified by the router
import HomeScreen from '../containers/HomeScreen/HomeScreen'
import SplashScreen from '../containers/SplashScreen'
import MasterScreen from '../containers/MasterScreen/MasterScreen'
import { Fonts, Colors } from '../GlobalConfig';
import ProfileScreen from '../containers/Profile/ProfileScreen'
import InfoScreen from '../containers/InfoScreen/InfoScreen'
import QRCodeScreen from '../containers/QRCodeScreen/QRCodeScreen'
import EditProfileScreen from '../containers/Profile/EditProfile/EditProfileScreen';
import MultiOptionScreen from '../containers/Utility/MultiOptionScreen';
import UpdateAppSceen from '../containers/UpdateAppScreen/UpdateAppSceen';

class NavigationRouter extends Component {
    showExitAlert() {
        Alert.alert(
            'Keluar Aplikasi',
            'Apakah Anda yakin untuk keluar aplikasi?',
            [
                {
                    text: 'Tidak',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'Ya', onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false },
        );
    }

    handleback = () => {
        let screen = Actions.currentScene;
        switch (screen) {
            case 'home':
                // this.showExitAlert()
                BackHandler.exitApp()
                return true;
            case 'master':
                BackHandler.exitApp()
                // this.showExitAlert()
                return true;
            case 'splash':
                BackHandler.exitApp()
                return true;
            case 'updateApp':
                BackHandler.exitApp()
                return true;
            default:
                Actions.pop()
                return true;
        }
    }

    render() {
        const MyTransitionSpec = ({
            duration: 250,
            easing: Easing.bezier(0.2833, 0.99, 0.31833, 0.99),
            // timing: Animated.timing,
        });

        const transitionConfig = () => ({
            transitionSpec: MyTransitionSpec,
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps;
                const { index } = scene;
                const width = layout.initWidth;

                // right to left by replacing bottom scene
                return {
                    transform: [{
                        translateX: position.interpolate({
                            inputRange: [index - 1, index, index + 1],
                            outputRange: [width, 0, -width],
                        }),
                    }]
                };
            }
        });

        return (
            <Router
                navigationBarStyle={{ backgroundColor: Colors.RED }}
                backAndroidHandler={this.handleback}>
                <Stack
                    transitionConfig={transitionConfig}
                    key='root'>
                    <Scene
                        initial
                        key='splash'
                        hideNavBar
                        component={SplashScreen} />
                    <Scene
                        key='updateApp'
                        hideNavBar
                        component={UpdateAppSceen} />
                    <Scene key='home'
                        hideNavBar
                        onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                        component={HomeScreen} />
                    <Scene key='master'
                        title="HOME"
                        titleStyle={styles.headerTitleBig}
                        type='reset'
                        component={MasterScreen} />
                    <Scene key='profil'
                        back
                        backButtonTintColor={Colors.WHITE}
                        title="PROFIL"
                        titleStyle={styles.headerTitleBigNoLeft}
                        component={ProfileScreen} />
                    <Scene key='editProfile'
                        back
                        backButtonTintColor={Colors.WHITE}
                        title="UBAH PROFIL"
                        titleStyle={styles.headerTitleBigNoLeft}
                        component={EditProfileScreen} />
                    <Scene key='info'
                        back
                        backButtonTintColor={Colors.WHITE}
                        title="INFO"
                        titleStyle={styles.headerTitleBigNoLeft}
                        component={InfoScreen} />
                    <Scene key='qrcode'
                        back
                        backButtonTintColor={Colors.WHITE}
                        titleStyle={styles.headerTitle}
                        component={QRCodeScreen} />
                    <Scene
                        back
                        backButtonTintColor={Colors.WHITE}
                        titleStyle={styles.headerTitle}
                        key='multiOption'
                        component={MultiOptionScreen} />
                </Stack>
            </Router>
        )
    }
}

const styles = StyleSheet.create({
    headerTitle: Platform.OS == 'android' ? {
        marginLeft: 0,
        color: Colors.WHITE,
        fontSize: 16,
        fontFamily: Fonts.APEX_REGULAR,
        letterSpacing: 0.7
    } : {
            color: Colors.WHITE,
            fontSize: 16,
            fontFamily: Fonts.APEX_REGULAR,
            letterSpacing: 0.7
        },
    headerTitleBig: {
        color: Colors.WHITE,
        fontFamily: Fonts.APEX_REGULAR,
        fontSize: 20,
        letterSpacing: 0.7
    },
    headerTitleBigNoLeft: Platform.OS == 'android' ? {
        marginLeft: 0,
        color: Colors.WHITE,
        fontFamily: Fonts.APEX_REGULAR,
        fontSize: 20,
        letterSpacing: 0.7
    } : {
            color: Colors.WHITE,
            fontFamily: Fonts.APEX_REGULAR,
            fontSize: 20,
            letterSpacing: 0.7
        }
})

export default NavigationRouter