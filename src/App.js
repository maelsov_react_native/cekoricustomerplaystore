import React, { Component } from 'react';
import { StatusBar, View } from 'react-native';
import { Provider } from 'react-redux';

import NavigationRouter from './navigations/NavigationRouter'
import { Colors } from './GlobalConfig';
import configureStore from "./redux/stores/store";

const store = configureStore();

export default class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<View style={{ flex: 1 }}>
					<StatusBar backgroundColor={Colors.RED_DARK} barStyle='light-content' />
					<NavigationRouter />
				</View>
			</Provider>
		);
	}
}
