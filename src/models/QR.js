import fetchNoCache from "../libraries/fetchNoCache"
import { APIList } from "../APIConfig"

export const modelQR = {
    scanQR: (post_data, update) => {
        return fetchNoCache(update, APIList.ScanQR.APIScanQRCode, 'POST', true, post_data)
    },
    checkQR: (data, update) => {
        let params_data = `qr_code=${data}`
        return fetchNoCache(update, APIList.ScanQR.APICheckQRCode, 'GET', true, '', params_data)
    },
    scanQRNew: (post_data, update) => {
        return fetchNoCache(update, APIList.ScanQR.APINewScanQRCode, 'POST', true, post_data)
    },
}