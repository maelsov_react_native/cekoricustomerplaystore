import fetchNoCache from "../libraries/fetchNoCache"
import { APIList } from "../APIConfig"

export const modelAuth = {
    login: (post_data, update) => {
        return fetchNoCache(update, APIList.Auth.APILogin, 'POST', false, post_data)
    },
    forgotPassword: (post_data, update) => {
        return fetchNoCache(update, APIList.Auth.APIForgotPassword, 'POST', false, post_data)
    },
    logout: (post_data, update) => {
        return fetchNoCache(update, APIList.Auth.APILogout, 'POST', false, post_data)
    },
}