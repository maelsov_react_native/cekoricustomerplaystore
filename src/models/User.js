import fetchNoCache from "../libraries/fetchNoCache"
import { APIList } from "../APIConfig"

export const modelUser = {
    register: (post_data, update) => {
        return fetchNoCache(update, APIList.User.APIRegister, 'POST', false, post_data)
    },
    getProfile: (user_id, update) => {
        const params_data = `user_id=${user_id}`
        return fetchNoCache(update, APIList.User.APIProfileDetail, 'GET', true, '', params_data)
    },
    updateAvatar: (post_data, update) => {
        return fetchNoCache(update, APIList.User.APIUpdateAvatar, 'POST', true, post_data)
    },
    updateProfile: (post_data, update) => {
        return fetchNoCache(update, APIList.User.APIUpdateProfile, 'POST', true, post_data)
    },
    checkUpdate: (params_data, update) => {
        return fetchNoCache(update, APIList.AppCheck.APICheckUpdate, 'GET', false, '', params_data)
    },
}