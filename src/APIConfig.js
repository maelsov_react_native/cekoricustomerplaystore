//Apps Version 0.1 ++
const APIDev1 = 'https://maelsov.id/cek_ori/api/v1/'
const APIProd1 = 'https://cekori.id/api/v1/'

//Apps Version 1.3 ++
const APIDev2 = 'https://maelsov.id/cek_ori/api/v2/'
const APIProd2 = 'https://cekori.id/api/v2/'

//Apps Version 1.3 ++
const APIDev3 = 'https://maelsov.id/cek_ori/api/v3/'
const APIProd3 = 'https://cekori.id/api/v3/'

const APIDev4 = 'http://206.189.81.185/cek_ori_v2/public/api/'



const BASE_URL_DEV = APIDev3

const BASE_URL_PROD = APIProd3

const BASE_URL = BASE_URL_PROD


const BASE_URL_NEW = APIDev4

const APIListAuth = {
    APILogin: `${BASE_URL}auth/login`,
    APILogout: `${BASE_URL}auth/logout`,
    APIForgotPassword: `${BASE_URL}auth/forget_password`,
}

const APIListUser = {
    APIRegister: `${BASE_URL}user/register`,
    APIUpdateAvatar: `${BASE_URL}user/update_user_avatar`,
    APIUpdateProfile: `${BASE_URL}user/update_profile`,
    APIProfileDetail: `${BASE_URL}user/profile`,
}

const APIListScanQR = {
    APIScanQRCode: `${BASE_URL}scan_qrcode/scan_qrcode`,
    APICheckQRCode: `${BASE_URL_NEW}transaction/old_qr/get`,
    APINewScanQRCode: `${BASE_URL_NEW}transaction/new_qr/post`,
    // APIScanQRCodeTest: `${BASE_URL_TEST}transaction/qr_zeta/get`,
    // APIScanQRCodeTest: `${BASE_URL_TEST}transaction/qr_alpha/get`
}

const APIListAPP = {
    APICheckUpdate: `${BASE_URL_NEW}version/update_priority/get`,
}


export const APIList = {
    Auth: APIListAuth,
    User: APIListUser,
    ScanQR: APIListScanQR,
    AppCheck : APIListAPP
}